﻿1
00:00:01,280 --> 00:00:06,640
So, let's start with a little bit

2
00:00:03,199 --> 00:00:10,719
of the history of coreboot

3
00:00:06,640 --> 00:00:11,440
so version one was created in 1999, as I

4
00:00:10,719 --> 00:00:15,519
said.

5
00:00:11,440 --> 00:00:19,359
Linux bios created by Ron Minnich

6
00:00:15,519 --> 00:00:22,640
that time he worked in Los Alamos

7
00:00:19,359 --> 00:00:25,279
Laboratory the goal was to boot Linux

8
00:00:22,640 --> 00:00:27,920
Linux as fast as possible and if

9
00:00:25,279 --> 00:00:31,679
possible to put Linux inside

10
00:00:27,920 --> 00:00:36,000
spy flash when normally BIOS

11
00:00:31,679 --> 00:00:37,440
was placed. So, he tried to replace BIOS

12
00:00:36,000 --> 00:00:40,000
with Linux.

13
00:00:37,440 --> 00:00:41,280
This project was called Linux BIOS and

14
00:00:40,000 --> 00:00:44,079
it was

15
00:00:41,280 --> 00:00:46,320
version one mainly

16
00:00:44,079 --> 00:00:50,800
operated in 1999,

17
00:00:46,320 --> 00:00:53,840
and 2000. Then we have another stage of

18
00:00:50,800 --> 00:00:57,360
Linux BIOS back then and future

19
00:00:53,840 --> 00:01:00,160
coreboot 2000 to 2005.

20
00:00:57,360 --> 00:01:02,719
Then we started to gain some additional

21
00:01:00,160 --> 00:01:06,240
support for various architectures

22
00:01:02,719 --> 00:01:09,600
x86 alpha power PC and

23
00:01:06,240 --> 00:01:11,439
then there were some

24
00:01:09,600 --> 00:01:14,640
problems with projects like

25
00:01:11,439 --> 00:01:17,840
the memory initialization causes some

26
00:01:14,640 --> 00:01:20,799
significant issues because the memory

27
00:01:17,840 --> 00:01:21,920
technology rise in complexity because of

28
00:01:20,799 --> 00:01:25,360
that

29
00:01:21,920 --> 00:01:28,720
ROMCC project was created to

30
00:01:25,360 --> 00:01:32,079
to change C into stackless up

31
00:01:28,720 --> 00:01:35,360
assembly and ROMCC kind of compiled C

32
00:01:32,079 --> 00:01:39,280
code in a way that  it use

33
00:01:35,360 --> 00:01:42,640
 registers for storing values

34
00:01:39,280 --> 00:01:45,439
as a stack. Then

35
00:01:42,640 --> 00:01:45,759
there was device 3 concept introduced

36
00:01:45,439 --> 00:01:48,720
by

37
00:01:45,759 --> 00:01:49,920
back in the days probably even before

38
00:01:48,720 --> 00:01:53,119
Linux

39
00:01:49,920 --> 00:01:56,000
device 3 concept. Then

40
00:01:53,119 --> 00:01:56,479
first payloads were introduced to

41
00:01:56,000 --> 00:01:59,280
make

42
00:01:56,479 --> 00:02:00,479
booting more flexible, in this period of

43
00:01:59,280 --> 00:02:02,479
time there was big

44
00:02:00,479 --> 00:02:04,799
contribution from various vendors from

45
00:02:02,479 --> 00:02:06,000
inter a lot of contribution and open

46
00:02:04,799 --> 00:02:09,759
source ajisa

47
00:02:06,000 --> 00:02:11,200
from AMD via SIS,

48
00:02:09,759 --> 00:02:13,280
there were many components

49
00:02:11,200 --> 00:02:15,520
contributing to that project back in the

50
00:02:13,280 --> 00:02:15,520
days.

51
00:02:15,920 --> 00:02:19,680
Then we had another stage  we can call

52
00:02:19,440 --> 00:02:23,680
it

53
00:02:19,680 --> 00:02:25,040
version 2 plus, it was 2005 to

54
00:02:23,680 --> 00:02:27,440


55
00:02:25,040 --> 00:02:28,720
where where there was introduction of

56
00:02:27,440 --> 00:02:32,400
cache as RAM,

57
00:02:28,720 --> 00:02:35,200
AMD 64 port

58
00:02:32,400 --> 00:02:37,840
ACPI and SMM support was added, there was

59
00:02:35,200 --> 00:02:41,040
flash on project created which helped in

60
00:02:37,840 --> 00:02:42,560
flashing Linux bios binaries or

61
00:02:41,040 --> 00:02:45,599
carbon binaries.

62
00:02:42,560 --> 00:02:46,239
Free software foundation was

63
00:02:45,599 --> 00:02:49,360
interested

64
00:02:46,239 --> 00:02:51,519
in coreboot and started use

65
00:02:49,360 --> 00:02:53,599
the Linux bios core boot on their own

66
00:02:51,519 --> 00:02:57,120
servers.

67
00:02:53,599 --> 00:03:01,200
iIn version 3, 2006

68
00:02:57,120 --> 00:03:05,280
2008 there was more than 100

69
00:03:01,200 --> 00:03:09,280
main boards supported but version 3 was

70
00:03:05,280 --> 00:03:13,519
a kind of experiment where some

71
00:03:09,280 --> 00:03:16,959
things were tried for example CBFS

72
00:03:13,519 --> 00:03:20,239
just to, so it was like

73
00:03:16,959 --> 00:03:22,720
it was like a

74
00:03:20,239 --> 00:03:25,120
wrong part of evolution or part of

75
00:03:22,720 --> 00:03:28,159
evolution that gave some ideas

76
00:03:25,120 --> 00:03:31,040
which were moved forward but the V3

77
00:03:28,159 --> 00:03:32,000
branch was killed they get back to V2

78
00:03:31,040 --> 00:03:35,519
and ported

79
00:03:32,000 --> 00:03:38,959
some most impo important

80
00:03:35,519 --> 00:03:42,879
improvements to V2 and then progress

81
00:03:38,959 --> 00:03:44,239
to V4. In 2008, Linux BIOS was renamed to

82
00:03:42,879 --> 00:03:46,159
coreboot,

83
00:03:44,239 --> 00:03:47,920
there was also change of the maintainer

84
00:03:46,159 --> 00:03:51,200
back then, Ron

85
00:03:47,920 --> 00:03:52,480
kind of  give place for Stefan

86
00:03:51,200 --> 00:03:57,599
Raynauer

87
00:03:52,480 --> 00:04:01,040
and in V4 from 2009 to 2012,

88
00:03:57,599 --> 00:04:04,640
they moved from SVN to GIT,

89
00:04:01,040 --> 00:04:08,319
there was also big contribution from AMD

90
00:04:04,640 --> 00:04:11,920
more against a staff and more

91
00:04:08,319 --> 00:04:12,239
hardware support. And finally, we

92
00:04:11,920 --> 00:04:16,560
are

93
00:04:12,239 --> 00:04:20,560
in version 4 and evolving

94
00:04:16,560 --> 00:04:21,280
since 2012, most important thing that

95
00:04:20,560 --> 00:04:24,560
kind of

96
00:04:21,280 --> 00:04:28,000
gave me a lot of

97
00:04:24,560 --> 00:04:31,040
thinking about coreboot was that

98
00:04:28,000 --> 00:04:31,919
google decided to use core boot as our

99
00:04:31,040 --> 00:04:34,960
chromebook

100
00:04:31,919 --> 00:04:38,240
default BIOS, this was huge

101
00:04:34,960 --> 00:04:41,280
boost for coreboot project.

102
00:04:38,240 --> 00:04:44,160
Intel created FSP,

103
00:04:41,280 --> 00:04:44,720
this was most probably because google

104
00:04:44,160 --> 00:04:47,280


105
00:04:44,720 --> 00:04:48,160
pushed Intel, that they don't want to

106
00:04:47,280 --> 00:04:52,320
use

107
00:04:48,160 --> 00:04:55,520
 this ami inside phoenix code

108
00:04:52,320 --> 00:04:55,520
so they decide to

109
00:04:55,600 --> 00:05:00,639
ask Intel to provide like smaller

110
00:04:58,639 --> 00:05:02,000
possible initialization code that could

111
00:05:00,639 --> 00:05:05,360
be integrated

112
00:05:02,000 --> 00:05:08,800
in coreboot.

113
00:05:05,360 --> 00:05:11,600
So, at that point I worked in Intel

114
00:05:08,800 --> 00:05:12,560
 and it was like blasting information

115
00:05:11,600 --> 00:05:15,680
for me,

116
00:05:12,560 --> 00:05:17,759
also couple additional

117
00:05:15,680 --> 00:05:21,360
things happened at that time

118
00:05:17,759 --> 00:05:22,639
there was WikiLeaks vault seven

119
00:05:21,360 --> 00:05:25,840
publications,

120
00:05:22,639 --> 00:05:29,360
there was Snowden revelations

121
00:05:25,840 --> 00:05:31,120
and many major security failures from

122
00:05:29,360 --> 00:05:34,240
big silicon conventors

123
00:05:31,120 --> 00:05:36,240
and ibvs. And probably because of that

124
00:05:34,240 --> 00:05:38,240
coreboot was more and more and more

125
00:05:36,240 --> 00:05:41,120
more popular and right now

126
00:05:38,240 --> 00:05:43,039
definitely we're going into stage of

127
00:05:41,120 --> 00:05:45,919
renaissance when

128
00:05:43,039 --> 00:05:48,639
major hardware vendors understand what

129
00:05:45,919 --> 00:05:52,320
coreboot is what value it provides.

130
00:05:48,639 --> 00:05:56,080
So, as you can see on this

131
00:05:52,320 --> 00:05:57,600
graphics on core boot website you

132
00:05:56,080 --> 00:06:00,720
can find

133
00:05:57,600 --> 00:06:01,199
all the releases and the dates. Recently

134
00:06:00,720 --> 00:06:04,800


135
00:06:01,199 --> 00:06:08,639
12 May  there was release of version

136
00:06:04,800 --> 00:06:12,160
four 4.12 the cadence of corbett release

137
00:06:08,639 --> 00:06:18,960
is 6 months and yes of course we should

138
00:06:12,160 --> 00:06:18,960
expect in November another release.


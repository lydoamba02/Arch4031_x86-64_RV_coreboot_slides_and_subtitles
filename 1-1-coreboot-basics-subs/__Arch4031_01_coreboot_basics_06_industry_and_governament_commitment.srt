﻿1
00:00:00,074 --> 00:00:01,014
Okay,

2
00:00:01,014 --> 00:00:03,003
so there is this chord with stuff,

3
00:00:03,003 --> 00:00:05,017
but who really do this,

4
00:00:05,017 --> 00:00:05,044
like,

5
00:00:05,045 --> 00:00:07,025
who cares about this project.

6
00:00:07,064 --> 00:00:11,000
So, there are many components that are involved,

7
00:00:11,001 --> 00:00:12,009
some of them very,

8
00:00:12,009 --> 00:00:12,087
very serious,

9
00:00:12,087 --> 00:00:15,076
like Google, Siemens, AMD, Intel, Mediatek,

10
00:00:16,014 --> 00:00:22,073
and we can also find some cool information about open

11
00:00:22,073 --> 00:00:26,004
compute project which is a place where

12
00:00:26,004 --> 00:00:27,076
Facebook and Google

13
00:00:27,077 --> 00:00:32,027
and right now many other organizations try to create open

14
00:00:32,027 --> 00:00:32,083
server,

15
00:00:32,083 --> 00:00:34,072
which gives

16
00:00:34,073 --> 00:00:38,005
well maintained and well maintained

17
00:00:38,005 --> 00:00:39,045
hardware for a long time.

18
00:00:39,084 --> 00:00:44,015
You can see even that official tests like GitHub account

19
00:00:44,016 --> 00:00:45,006
contained coreboot fork,

20
00:00:45,061 --> 00:00:47,096
which means maybe they using it

21
00:00:47,097 --> 00:00:49,005
for some reasons.

22
00:00:49,051 --> 00:00:54,007
There are situations where some coreboot the Census Service

23
00:00:54,007 --> 00:00:59,027
providers are hired by some some customers,

24
00:00:59,028 --> 00:01:00,005
for example,

25
00:01:00,005 --> 00:01:02,066
in case of Port of Super Micro

26
00:01:02,074 --> 00:01:04,067
,

27
00:01:04,068 --> 00:01:07,048
one company was hired by VPN provider

28
00:01:07,049 --> 00:01:07,081


29
00:01:07,082 --> 00:01:10,026
it's very interesting that,

30
00:01:10,094 --> 00:01:12,024
for example,

31
00:01:12,024 --> 00:01:15,048
VPN providers see the value in open source firmware

32
00:01:15,049 --> 00:01:16,007
So,

33
00:01:16,071 --> 00:01:17,096
why coreboot?

34
00:01:17,097 --> 00:01:18,096
It's free,

35
00:01:18,097 --> 00:01:21,022
as free beer,

36
00:01:21,023 --> 00:01:23,014
it's very simple design,

37
00:01:23,015 --> 00:01:27,095
it does minimum booting to a minimum

38
00:01:28,044 --> 00:01:30,045
what is needed to boot OS,

39
00:01:30,046 --> 00:01:30,075


40
00:01:30,076 --> 00:01:32,024
it's vendor independent,

41
00:01:32,025 --> 00:01:33,006
it's cross platform,

42
00:01:33,061 --> 00:01:35,005
it's very fast,

43
00:01:35,006 --> 00:01:38,018
it's way easier than UEFI.

44
00:01:38,018 --> 00:01:44,056
In comparison to legacy BIOS written in assembly

45
00:01:44,057 --> 00:01:45,034


46
00:01:45,035 --> 00:01:47,018
the code quality is much better,

47
00:01:47,018 --> 00:01:48,099
is much more flexible.

48
00:01:49,000 --> 00:01:49,036


49
00:01:49,037 --> 00:01:51,005
It's very important because,

50
00:01:51,006 --> 00:01:51,058


51
00:01:51,059 --> 00:01:54,044
many companies want to customize firmware

52
00:01:54,045 --> 00:01:54,087


53
00:01:54,088 --> 00:01:58,086
in ways that it will work in special way

54
00:01:58,086 --> 00:01:59,096
on their hardware

55
00:02:00,004 --> 00:02:00,081


56
00:02:00,082 --> 00:02:01,069
and typically,

57
00:02:01,069 --> 00:02:04,087
this is not supported by BIOS or,

58
00:02:04,087 --> 00:02:05,009


59
00:02:05,009 --> 00:02:06,006
UEFI vendors.

60
00:02:06,074 --> 00:02:07,046
Of course,

61
00:02:07,047 --> 00:02:09,075
you may pay them like Big Buck,

62
00:02:09,075 --> 00:02:10,096
but also,

63
00:02:10,096 --> 00:02:13,066
you can go to open source firmware and take open

64
00:02:13,066 --> 00:02:16,096
source firmware and modify optimized performance

65
00:02:16,097 --> 00:02:17,056


66
00:02:17,057 --> 00:02:18,002
like,

67
00:02:18,002 --> 00:02:20,009
do some more power saving art,

68
00:02:20,009 --> 00:02:21,021
security,

69
00:02:21,022 --> 00:02:22,006
disabled some features.

70
00:02:22,006 --> 00:02:23,016
For example

71
00:02:23,016 --> 00:02:28,031
there was story from Siemens that they had CNC machine

72
00:02:28,031 --> 00:02:31,006
that they wanted to disable SMS,

73
00:02:31,006 --> 00:02:32,086
and it was impossible in UEFI,

74
00:02:33,024 --> 00:02:37,001
because those Siemens cause some delays in in CNC

75
00:02:37,001 --> 00:02:40,095
machines wouldmake it less precise.

76
00:02:41,034 --> 00:02:42,006


77
00:02:42,054 --> 00:02:45,075
And coreboot is cross platform,

78
00:02:46,074 --> 00:02:49,017
it's not only possible to run on one machine,

79
00:02:49,018 --> 00:02:50,069
many machines can be run,

80
00:02:50,007 --> 00:02:53,017
it's easy to port relatively

81
00:02:53,018 --> 00:02:54,052
if you will go through training,

82
00:02:54,052 --> 00:02:57,066
definitely you can understand how to do that.

83
00:02:58,014 --> 00:03:02,042
And we recently we see even more commitment so you

84
00:03:02,042 --> 00:03:07,008
can see that Purism laptop producer, ??? laptop producer ,

85
00:03:07,081 --> 00:03:12,096
System 76 another provider of laptops loves open source,

86
00:03:12,096 --> 00:03:14,046
filmer and coreboot.

87
00:03:14,046 --> 00:03:18,026
We also see companies like in Insurgo,

88
00:03:18,027 --> 00:03:22,068
which provide QubesOS certified laptops

89
00:03:22,069 --> 00:03:23,035
,

90
00:03:23,074 --> 00:03:30,006
look at coreboot as valuable boot system and and

91
00:03:30,006 --> 00:03:34,061
also firewall providers like Protect Lee PC engines,

92
00:03:34,062 --> 00:03:40,017
those companies see value and their customers value in coreboot

93
00:03:40,017 --> 00:03:43,026
as a BIOS for those.

94
00:03:43,084 --> 00:03:45,043
If that is not enough,

95
00:03:45,044 --> 00:03:48,007
there is even a government commitment.

96
00:03:48,008 --> 00:03:48,038
So,

97
00:03:48,038 --> 00:03:49,035
for example,

98
00:03:49,035 --> 00:03:53,003
there is a federal office for information security in Germany

99
00:03:53,031 --> 00:03:54,005


100
00:03:54,006 --> 00:03:55,056
it's good example

101
00:03:55,057 --> 00:03:57,083
and like representatives of

102
00:03:57,084 --> 00:03:58,004


103
00:03:58,041 --> 00:04:05,026
this agency frequently attended coreboot conference officer spindle conference

104
00:04:05,044 --> 00:04:06,003


105
00:04:06,004 --> 00:04:09,073
saying that they value coreboot, because they can fork

106
00:04:09,073 --> 00:04:10,019
it

107
00:04:10,002 --> 00:04:10,007


108
00:04:10,071 --> 00:04:12,015
it's secure,

109
00:04:12,015 --> 00:04:14,019
they are not depending on anyone,

110
00:04:14,002 --> 00:04:15,075
it's easy to out it,

111
00:04:15,075 --> 00:04:20,011
they can check the code that runs on government administration

112
00:04:20,012 --> 00:04:21,041
machines.

113
00:04:21,042 --> 00:04:22,022
And that,

114
00:04:22,023 --> 00:04:23,005
and that is very,

115
00:04:23,005 --> 00:04:24,055
very important for them

116
00:04:24,074 --> 00:04:29,006
There is even an NSA contribution to coreboot,

117
00:04:29,061 --> 00:04:36,015
very interesting contribution about SMI transfer monitor for Intel.

118
00:04:36,016 --> 00:04:37,058
Interestingly,

119
00:04:37,059 --> 00:04:42,067
there are some presentation where a person who imported STM

120
00:04:42,068 --> 00:04:47,001
said that someone asked what is needed to support a

121
00:04:47,001 --> 00:04:48,013
semi transfer monitor,

122
00:04:48,013 --> 00:04:51,045
which is advanced security feature of Intel platforms,

123
00:04:51,046 --> 00:04:55,043
he say, "You need SDM friendly BIOS."

124
00:04:55,044 --> 00:04:58,099
So, coreboot is definitely SDM friendly BIOS,

125
00:04:59,000 --> 00:05:01,049
not UEFI implementations,

126
00:05:01,049 --> 00:05:04,025
not some old BIOS implementations.


﻿1
00:00:00,399 --> 00:00:04,240
So, what are the development rules. So

2
00:00:03,199 --> 00:00:06,240
in most cases

3
00:00:04,240 --> 00:00:07,600
you should branch from

4
00:00:06,240 --> 00:00:10,880
master branch, of course

5
00:00:07,600 --> 00:00:14,160
this is like a general rule of ???

6
00:00:10,880 --> 00:00:15,519
but in some situation master may be

7
00:00:14,160 --> 00:00:17,440
broken or

8
00:00:15,519 --> 00:00:19,039
master maybe not suitable for your

9
00:00:17,440 --> 00:00:23,359
development.

10
00:00:19,039 --> 00:00:27,599
So, you then fork from the latest

11
00:00:23,359 --> 00:00:29,359
release and you based on stable

12
00:00:27,599 --> 00:00:31,599
release, of course there is no

13
00:00:29,359 --> 00:00:32,640
quality checks on the releases, I

14
00:00:31,599 --> 00:00:36,239
believe there are just

15
00:00:32,640 --> 00:00:39,760
a brief checkpoint

16
00:00:36,239 --> 00:00:41,680
without any significant meaning in coreboor

17
00:00:39,760 --> 00:00:44,879
release process.

18
00:00:41,680 --> 00:00:45,520
Then second thing what is

19
00:00:44,879 --> 00:00:48,239
important

20
00:00:45,520 --> 00:00:49,840
is the code style, it follows Linux

21
00:00:48,239 --> 00:00:52,559
kernel coding style,

22
00:00:49,840 --> 00:00:53,440
if you don't know it then you

23
00:00:52,559 --> 00:00:57,280
definitely should

24
00:00:53,440 --> 00:01:00,079
read through it, it's well documented

25
00:00:57,280 --> 00:01:00,640
like most of the open source projects

26
00:01:00,079 --> 00:01:03,039
follow

27
00:01:00,640 --> 00:01:05,360
Linux kernel coding style. There are some

28
00:01:03,039 --> 00:01:08,799
minor difference in coreboot

29
00:01:05,360 --> 00:01:12,880
for example aligned with it's not

30
00:01:08,799 --> 00:01:15,680
80 characters it's 96 characters

31
00:01:12,880 --> 00:01:16,880
but on on the other side it still

32
00:01:15,680 --> 00:01:19,840
follows

33
00:01:16,880 --> 00:01:22,000
karingam and ritchie code style, it use

34
00:01:19,840 --> 00:01:25,759
fixed size variable types like

35
00:01:22,000 --> 00:01:28,720
uint or u32.

36
00:01:25,759 --> 00:01:30,960
If you paste code from some other

37
00:01:28,720 --> 00:01:34,240
location, you definitely should check

38
00:01:30,960 --> 00:01:37,439
license to not break any any rules.

39
00:01:34,240 --> 00:01:40,880
If you're doing any assembly

40
00:01:37,439 --> 00:01:43,439
you should separate

41
00:01:40,880 --> 00:01:46,799
assembly code

42
00:01:43,439 --> 00:01:47,439
to some of additional files. You should

43
00:01:46,799 --> 00:01:49,759
avoid

44
00:01:47,439 --> 00:01:50,560
inlining assembly or maybe to some

45
00:01:49,759 --> 00:01:53,200
functions,

46
00:01:50,560 --> 00:01:54,720
you should avoid inlining assembly

47
00:01:53,200 --> 00:01:55,280
and when you're writing assembly you

48
00:01:54,720 --> 00:01:58,719
should you

49
00:01:55,280 --> 00:02:01,360
use AT&T syntax for so what can be

50
00:01:58,719 --> 00:02:03,119
sometimes problematic for

51
00:02:01,360 --> 00:02:06,960
people used to

52
00:02:03,119 --> 00:02:08,399
other syntax. All functions must have

53
00:02:06,960 --> 00:02:11,520
prototypes,

54
00:02:08,399 --> 00:02:13,360
local functions must be static,

55
00:02:11,520 --> 00:02:15,520
and there should be no function

56
00:02:13,360 --> 00:02:17,280
definitions in headers.

57
00:02:15,520 --> 00:02:19,280
There is

58
00:02:17,280 --> 00:02:21,520
much more to that,

59
00:02:19,280 --> 00:02:23,040
but those are basic rules that you

60
00:02:21,520 --> 00:02:23,760
should follow that you should know if

61
00:02:23,040 --> 00:02:29,120
you

62
00:02:23,760 --> 00:02:29,120
do some simple changes.


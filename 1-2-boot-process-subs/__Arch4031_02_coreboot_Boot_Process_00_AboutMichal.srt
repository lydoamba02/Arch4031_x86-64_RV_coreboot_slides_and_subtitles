﻿1
00:00:00,000 --> 00:00:01,350
A little bit about me,

2
00:00:01,350 --> 00:00:03,250
my name is Michał Żygowski and,

3
00:00:03,250 --> 00:00:05,800
I am a firmware engineer at 3mdeb

4
00:00:05,800 --> 00:00:07,450
embedded systems consulting.

5
00:00:08,240 --> 00:00:10,700
I am a Braswell SoC maintainer,

6
00:00:10,700 --> 00:00:12,050
as well as PC Engines

7
00:00:12,050 --> 00:00:14,850
and Protectli platforms maintainer in coreboot.

8
00:00:15,650 --> 00:00:17,550
I am mainly interested in

9
00:00:17,550 --> 00:00:24,320
advanced hardware and firmware features,

10
00:00:20,000 --> 00:00:24,320
coreboot, and security solutions.


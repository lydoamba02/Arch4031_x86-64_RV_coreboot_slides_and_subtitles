﻿1
00:00:00,799 --> 00:00:05,200
So, let's move to the first stage of

2
00:00:02,720 --> 00:00:07,120
coreboot which is called boot block.

3
00:00:05,200 --> 00:00:09,519
This is where the processor jumps after

4
00:00:07,120 --> 00:00:12,480
the reset vector

5
00:00:09,519 --> 00:00:14,000
since it is very early boot stage there

6
00:00:12,480 --> 00:00:15,599
is quite much assembly code at the

7
00:00:14,000 --> 00:00:17,760
beginning because

8
00:00:15,599 --> 00:00:19,039
the environment is quite poor, you have

9
00:00:17,760 --> 00:00:22,000
to execute

10
00:00:19,039 --> 00:00:23,519
in place from the ROM because the RAM is

11
00:00:22,000 --> 00:00:26,560
not ready yet.

12
00:00:23,519 --> 00:00:27,760
So, to the main task of coreboot we may

13
00:00:26,560 --> 00:00:30,960
include

14
00:00:27,760 --> 00:00:33,279
switching the CPU to 32-bit protected

15
00:00:30,960 --> 00:00:33,279
mode

16
00:00:33,440 --> 00:00:37,920
and we perform some very basic

17
00:00:35,760 --> 00:00:40,000
initialization of the silicon,

18
00:00:37,920 --> 00:00:41,440
just enough to enable the debug

19
00:00:40,000 --> 00:00:45,039
interface and

20
00:00:41,440 --> 00:00:47,280
set up the temporary memory, the HIP and

21
00:00:45,039 --> 00:00:49,520
the stack in the cache.

22
00:00:47,280 --> 00:00:53,039
This process of a new temporary memory

23
00:00:49,520 --> 00:00:56,640
is called cache's RAM initialization.

24
00:00:53,039 --> 00:00:56,960
Why cache's RAM? In the very first days

25
00:00:56,640 --> 00:01:00,079
of

26
00:00:56,960 --> 00:01:01,760
the BIOS and firmware it was mainly

27
00:01:00,079 --> 00:01:04,479
written in the assembly,

28
00:01:01,760 --> 00:01:07,760
this assembly code was responsible for

29
00:01:04,479 --> 00:01:10,320
early silicon setup and memory training

30
00:01:07,760 --> 00:01:12,159
but after the memory was ready only then

31
00:01:10,320 --> 00:01:14,720
the higher level programming

32
00:01:12,159 --> 00:01:15,759
language could be used to write the code.

33
00:01:14,720 --> 00:01:18,080
Over time,

34
00:01:15,759 --> 00:01:20,080
with the release of newer specifications

35
00:01:18,080 --> 00:01:22,640
of DDR memory,

36
00:01:20,080 --> 00:01:23,680
the memory installation has become so

37
00:01:22,640 --> 00:01:26,000
complex

38
00:01:23,680 --> 00:01:28,000
that it was too difficult to write

39
00:01:26,000 --> 00:01:30,560
purely in assembly,

40
00:01:28,000 --> 00:01:31,520
that is why the CAR mode has been

41
00:01:30,560 --> 00:01:33,600
introduced

42
00:01:31,520 --> 00:01:35,520
which lets the firmware developers use

43
00:01:33,600 --> 00:01:38,560
the higher level programming language

44
00:01:35,520 --> 00:01:39,280
SC in the early boot stages when RAM is

45
00:01:38,560 --> 00:01:41,840
not yet

46
00:01:39,280 --> 00:01:41,840
ready.


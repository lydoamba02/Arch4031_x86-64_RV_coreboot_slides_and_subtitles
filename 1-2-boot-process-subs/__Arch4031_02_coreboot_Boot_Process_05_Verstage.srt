﻿1
00:00:00,080 --> 00:00:05,680
So, how

2
00:00:03,199 --> 00:00:06,399
the core boot image layout may look like

3
00:00:05,680 --> 00:00:08,880
with

4
00:00:06,399 --> 00:00:10,080
vboot. So, basically there are many

5
00:00:08,880 --> 00:00:13,440
combinations

6
00:00:10,080 --> 00:00:16,560
of CBFS and flash mobile playoffs with

7
00:00:13,440 --> 00:00:19,840
people and there are actually

8
00:00:16,560 --> 00:00:22,160
three main ones. We can have only a read

9
00:00:19,840 --> 00:00:24,480
only part of CBFS which is

10
00:00:22,160 --> 00:00:25,359
also equivalent to a standard coreboot

11
00:00:24,480 --> 00:00:27,840
build

12
00:00:25,359 --> 00:00:30,480
and you can have and read-only partition

13
00:00:27,840 --> 00:00:33,760
with one required partition of

14
00:00:30,480 --> 00:00:35,600
or with two read-write partitions. So,

15
00:00:33,760 --> 00:00:37,600
each of these partitions read-only

16
00:00:35,600 --> 00:00:41,840
and rewrite should contain

17
00:00:37,600 --> 00:00:44,559
at least one CBFS partition inside.

18
00:00:41,840 --> 00:00:47,280
So, reboot enforces this specifically

19
00:00:44,559 --> 00:00:49,200
on the brush map

20
00:00:47,280 --> 00:00:50,719
there is only part that is typically called

21
00:00:49,200 --> 00:00:52,879
recovery

22
00:00:50,719 --> 00:00:54,960
and the read-write our updatable

23
00:00:52,879 --> 00:00:58,239
firmware.

24
00:00:54,960 --> 00:01:01,600
So, it works like this that

25
00:00:58,239 --> 00:01:03,520
each of these partitions

26
00:01:01,600 --> 00:01:05,280
contain copies of the executable

27
00:01:03,520 --> 00:01:06,000
stages that are necessary to proceed

28
00:01:05,280 --> 00:01:08,960
from

29
00:01:06,000 --> 00:01:10,320
the boot block and first stage. Okay,

30
00:01:08,960 --> 00:01:12,000
let's start with

31
00:01:10,320 --> 00:01:13,760
at the beginning of the boot process

32
00:01:12,000 --> 00:01:15,600
which starts with the putback and recent

33
00:01:13,760 --> 00:01:18,400
vector from read only part

34
00:01:15,600 --> 00:01:20,159
always. Because, this is the part that

35
00:01:18,400 --> 00:01:23,200
lies

36
00:01:20,159 --> 00:01:23,920
at the top of the flash part which is

37
00:01:23,200 --> 00:01:26,080
mapped at

38
00:01:23,920 --> 00:01:27,280
under the four gigabytes, so here we

39
00:01:26,080 --> 00:01:30,320
start with the

40
00:01:27,280 --> 00:01:33,600
boot block in the read only part

41
00:01:30,320 --> 00:01:34,960
which then handles over the control to

42
00:01:33,600 --> 00:01:37,439
the first stage

43
00:01:34,960 --> 00:01:39,600
which is responsible for verifying the

44
00:01:37,439 --> 00:01:41,680
read write parts of the firmware.

45
00:01:39,600 --> 00:01:43,040
So, basically the web bootlogic in the

46
00:01:41,680 --> 00:01:46,560
first stage

47
00:01:43,040 --> 00:01:47,759
checks for the vblock A and vblock

48
00:01:46,560 --> 00:01:51,119
B,

49
00:01:47,759 --> 00:01:54,240
and first in the order is the slot A

50
00:01:51,119 --> 00:01:57,360
and checks whether the signatures

51
00:01:54,240 --> 00:01:57,360
in the vblock A

52
00:01:57,600 --> 00:02:02,880
match the hashes and signatures

53
00:02:01,119 --> 00:02:05,840
in the vblock it matches the ones

54
00:02:02,880 --> 00:02:05,840
calculated from the

55
00:02:06,079 --> 00:02:10,560
firmware main A partition containing the

56
00:02:08,160 --> 00:02:12,640
CBFS,

57
00:02:10,560 --> 00:02:14,560
and it does the verification and if the

58
00:02:12,640 --> 00:02:17,920
verification passes

59
00:02:14,560 --> 00:02:20,640
the slot A is marked as good and

60
00:02:17,920 --> 00:02:21,280
the first stage hands over the control

61
00:02:20,640 --> 00:02:24,640
to the

62
00:02:21,280 --> 00:02:27,920
wrong stage part in this partition.

63
00:02:24,640 --> 00:02:29,920
If the slot A is not valid and the

64
00:02:27,920 --> 00:02:31,280
verification of the signature and

65
00:02:29,920 --> 00:02:34,400
has failed

66
00:02:31,280 --> 00:02:36,800
the ??? will try the partition B

67
00:02:34,400 --> 00:02:38,080
and perform the similar operation as

68
00:02:36,800 --> 00:02:41,440
with vblock A

69
00:02:38,080 --> 00:02:43,680
and try to verify this firmware main B

70
00:02:41,440 --> 00:02:47,120
partition.

71
00:02:43,680 --> 00:02:49,519
If it succeeds the first stage will pass

72
00:02:47,120 --> 00:02:50,800
the control to the ROM station partition

73
00:02:49,519 --> 00:02:52,959
B

74
00:02:50,800 --> 00:02:54,879
and rounds will continue to subsequent

75
00:02:52,959 --> 00:02:56,720
stages.

76
00:02:54,879 --> 00:02:58,159
At the end of course we have some kind

77
00:02:56,720 --> 00:03:02,400
of payload

78
00:02:58,159 --> 00:03:05,840
as well as in the partition A and B

79
00:03:02,400 --> 00:03:07,760
which then loads the target operating

80
00:03:05,840 --> 00:03:10,959
system.

81
00:03:07,760 --> 00:03:12,400
However, if either slot A

82
00:03:10,959 --> 00:03:15,519
nor slot B

83
00:03:12,400 --> 00:03:16,840
is good, then the first stage will hand

84
00:03:15,519 --> 00:03:20,080
over

85
00:03:16,840 --> 00:03:23,360
to the ROM stage and the

86
00:03:20,080 --> 00:03:24,080
read-only part and mark that a recovery

87
00:03:23,360 --> 00:03:28,000


88
00:03:24,080 --> 00:03:31,120
is needed. So, it will

89
00:03:28,000 --> 00:03:34,720
so it will mark the recovery request and

90
00:03:31,120 --> 00:03:37,120
reboot the platform. After the reboot the

91
00:03:34,720 --> 00:03:39,519
boot block will also be

92
00:03:37,120 --> 00:03:40,879
launched from the read-only part then go

93
00:03:39,519 --> 00:03:43,280
to the first stage

94
00:03:40,879 --> 00:03:44,840
and the vboot logic in first stage when

95
00:03:43,280 --> 00:03:48,000
it detects the

96
00:03:44,840 --> 00:03:49,440
recovery, it directly passes

97
00:03:48,000 --> 00:03:52,400
the control to the romstage

98
00:03:49,440 --> 00:03:55,760
read-only part.

99
00:03:52,400 --> 00:03:57,920
So, then the romstage launches

100
00:03:55,760 --> 00:04:01,519
subsequent

101
00:03:57,920 --> 00:04:04,239
stages like romstage and payload.

102
00:04:01,519 --> 00:04:05,760
And then the final payload in

103
00:04:04,239 --> 00:04:09,120
read-only part may

104
00:04:05,760 --> 00:04:12,560
launch some recovery operating system.

105
00:04:09,120 --> 00:04:14,400
It must be noted that the recovery part

106
00:04:12,560 --> 00:04:16,479
in the read-only partition is not

107
00:04:14,400 --> 00:04:22,000
protected by any signature, so

108
00:04:16,479 --> 00:04:22,000
this code is executed unconditionally.


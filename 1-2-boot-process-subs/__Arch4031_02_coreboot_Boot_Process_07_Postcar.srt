﻿1
00:00:00,150 --> 00:00:02,300
The next stage after the romstage

2
00:00:02,400 --> 00:00:04,000
is called postcar.

3
00:00:04,700 --> 00:00:06,900
This stage has been designed to create a

4
00:00:06,900 --> 00:00:08,750
clear program boundary between the romstage

5
00:00:08,750 --> 00:00:09,900
and ramstage.

6
00:00:10,350 --> 00:00:12,350
Before postcar has been introduced,

7
00:00:12,750 --> 00:00:15,500
there were, so-called CAR globals

8
00:00:15,500 --> 00:00:16,450
which were,

9
00:00:16,450 --> 00:00:18,250
variables that had to be stored

10
00:00:18,250 --> 00:00:20,250
in a special section of ramstage

11
00:00:20,250 --> 00:00:21,300
and copied over

12
00:00:21,600 --> 00:00:24,100
when the CAR has been teared down.

13
00:00:25,150 --> 00:00:27,950
It was quite a burden for developers to

14
00:00:27,950 --> 00:00:29,400
maintain that solution

15
00:00:30,000 --> 00:00:33,570
so, CAR teardown has been moved to a separate stage.

16
00:00:35,200 --> 00:00:38,250
This stage has a very few responsibilities

17
00:00:38,250 --> 00:00:41,100
and takes a very short amount of

18
00:00:41,100 --> 00:00:42,800
time during the boot process.

19
00:00:43,750 --> 00:00:45,300
It is still executed in

20
00:00:45,300 --> 00:00:46,700
temporary memory environment

21
00:00:46,700 --> 00:00:47,800
the Cache-As-RAM,

22
00:00:48,400 --> 00:00:49,850
and mainly uses C code,

23
00:00:49,850 --> 00:00:53,350
with certain elements of assembly during the teardown.

24
00:00:54,700 --> 00:00:57,050
The main tasks of postcar are,

25
00:00:57,050 --> 00:00:59,780
to tear the Cache-As-RAM environment down,

26
00:00:59,780 --> 00:01:02,050
switch to the RAM, the main memory

27
00:01:03,140 --> 00:01:05,750
and to load and execute the ramstage.


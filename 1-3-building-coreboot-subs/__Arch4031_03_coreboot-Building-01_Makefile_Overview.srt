﻿1
00:00:00,399 --> 00:00:04,319
Okay, let's start with the make file

2
00:00:02,480 --> 00:00:07,200
brief overview.

3
00:00:04,319 --> 00:00:08,720
Makefile is a file that is utilized by

4
00:00:07,200 --> 00:00:11,280
GNU make tool.

5
00:00:08,720 --> 00:00:13,519
In general, it contains information how

6
00:00:11,280 --> 00:00:14,960
to produce output files from several

7
00:00:13,519 --> 00:00:17,199
input files.

8
00:00:14,960 --> 00:00:19,039
It can be used for a variety of tasks

9
00:00:17,199 --> 00:00:22,480
but of course, we will focus on the

10
00:00:19,039 --> 00:00:23,519
source code complying, make file basic

11
00:00:22,480 --> 00:00:26,080
syntax

12
00:00:23,519 --> 00:00:29,039
may already be familiar for you but

13
00:00:26,080 --> 00:00:32,719
let's remind it for completeness anyway.

14
00:00:29,039 --> 00:00:36,640
On the left side we have the output

15
00:00:32,719 --> 00:00:39,280
file so-called target and after a column

16
00:00:36,640 --> 00:00:41,680
we have a list of input files also known

17
00:00:39,280 --> 00:00:44,239
as dependency list.

18
00:00:41,680 --> 00:00:45,440
Below, there are set of rules describing

19
00:00:44,239 --> 00:00:48,160
how to produce

20
00:00:45,440 --> 00:00:49,120
target file out of input files. The

21
00:00:48,160 --> 00:00:51,520
important part

22
00:00:49,120 --> 00:00:52,160
to remember is to use a tabulation as

23
00:00:51,520 --> 00:00:55,920
indent

24
00:00:52,160 --> 00:00:57,520
not spaces. Let's look at the example of

25
00:00:55,920 --> 00:01:02,160
make hub in coreboot

26
00:00:57,520 --> 00:01:02,160
that would execute the target hub.

27
00:01:04,320 --> 00:01:07,600
I have already opened the main make file

28
00:01:07,200 --> 00:01:12,400
and

29
00:01:07,600 --> 00:01:15,840
coreboot. So, here we have the

30
00:01:12,400 --> 00:01:19,360
help target which is

31
00:01:15,840 --> 00:01:19,680
called we have coreboot and as you can

32
00:01:19,360 --> 00:01:22,880
see

33
00:01:19,680 --> 00:01:27,840
it just it is just echoing the usage of

34
00:01:22,880 --> 00:01:27,840
make in the corporate repository.

35
00:01:29,840 --> 00:01:34,479
So, as you could see there is one main

36
00:01:32,880 --> 00:01:35,680
make file in the root directory of

37
00:01:34,479 --> 00:01:38,400
coreboot

38
00:01:35,680 --> 00:01:39,680
and this is really the only one that

39
00:01:38,400 --> 00:01:42,720
lies within the corporate

40
00:01:39,680 --> 00:01:44,880
directory and almost whole source. There

41
00:01:42,720 --> 00:01:45,680
are also other separate make files as an

42
00:01:44,880 --> 00:01:47,840
exception

43
00:01:45,680 --> 00:01:50,159
for building some utility tools,

44
00:01:47,840 --> 00:01:52,720
libraries and payloads.

45
00:01:50,159 --> 00:01:54,640
Apart from that there are many in-cloud

46
00:01:52,720 --> 00:01:57,600
make files

47
00:01:54,640 --> 00:01:58,159
named makefile.inc files that lie in

48
00:01:57,600 --> 00:02:01,520
nearly

49
00:01:58,159 --> 00:02:08,399
each sourcetree subdirectory. So,

50
00:02:01,520 --> 00:02:10,560
let's see where are they placed.

51
00:02:08,399 --> 00:02:11,440
So, as you can see in the root directory

52
00:02:10,560 --> 00:02:15,599
there is also

53
00:02:11,440 --> 00:02:15,599
the makefile.inc file in the redirectory

54
00:02:15,680 --> 00:02:20,720
and it sources other directories of

55
00:02:18,640 --> 00:02:24,000
coreboot in order to look

56
00:02:20,720 --> 00:02:26,080
for other make files. But, I will explain

57
00:02:24,000 --> 00:02:29,360
it a little bit later.

58
00:02:26,080 --> 00:02:32,400
So, let's have a look for example at

59
00:02:29,360 --> 00:02:34,080
other make files let's go to the

60
00:02:32,400 --> 00:02:37,280
payloads directory

61
00:02:34,080 --> 00:02:40,160
and yet we found another one

62
00:02:37,280 --> 00:02:40,160
make file file.

63
00:02:41,120 --> 00:02:48,000
If we enter for example another

64
00:02:44,160 --> 00:02:48,000
directory let's say external,

65
00:02:48,400 --> 00:02:55,440
we find another one, etc, etc.

66
00:02:52,959 --> 00:02:56,879
We can find many other make files if we

67
00:02:55,440 --> 00:03:00,480
look for them,

68
00:02:56,879 --> 00:03:00,480
for example let's say

69
00:03:00,879 --> 00:03:05,680
security, another makefile

70
00:03:07,360 --> 00:03:17,280
and that's how other make files are

71
00:03:11,040 --> 00:03:17,280
placed all over the source in the coreboot.


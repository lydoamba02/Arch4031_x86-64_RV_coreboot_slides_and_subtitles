﻿1
00:00:00,719 --> 00:00:05,440
Now that we have some basic knowledge

2
00:00:02,720 --> 00:00:07,040
about the make files and kconfig system,

3
00:00:05,440 --> 00:00:08,559
let's try out some basic commands

4
00:00:07,040 --> 00:00:10,559
related to them.

5
00:00:08,559 --> 00:00:12,960
As a quick example, let's invoke make

6
00:00:10,559 --> 00:00:15,120
help from the core boot directory.

7
00:00:12,960 --> 00:00:17,920
It will give us quite good overview what

8
00:00:15,120 --> 00:00:20,160
parameters make may take in coreboot,

9
00:00:17,920 --> 00:00:20,960
in the output we will notice the

10
00:00:20,160 --> 00:00:23,519
platform

11
00:00:20,960 --> 00:00:25,519
kconfig, toolchain and coreboot test

12
00:00:23,519 --> 00:00:28,720
targets.

13
00:00:25,519 --> 00:00:31,760
So, now let's try to invoke it, I

14
00:00:28,720 --> 00:00:35,680
am already in the coreboot directory,

15
00:00:31,760 --> 00:00:35,680
so I will just type make help

16
00:00:35,840 --> 00:00:39,360
and now as we can see it printed out the

17
00:00:38,559 --> 00:00:43,040
possible

18
00:00:39,360 --> 00:00:44,480
make parameters. And as I said this

19
00:00:43,040 --> 00:00:45,840
desire are divided into coreboot

20
00:00:44,480 --> 00:00:48,640
platform targets,

21
00:00:45,840 --> 00:00:51,840
kconfig targets, toolchain targets and

22
00:00:48,640 --> 00:00:51,840
coreboot test targets.

23
00:00:55,440 --> 00:00:59,280
In the previous lesson, I have shown you

24
00:00:57,440 --> 00:01:00,960
how to invoke make and

25
00:00:59,280 --> 00:01:02,879
how to look for the parameters which

26
00:01:00,960 --> 00:01:04,479
make may take,

27
00:01:02,879 --> 00:01:06,080
let's review now the most important

28
00:01:04,479 --> 00:01:09,200
commands from

29
00:01:06,080 --> 00:01:10,320
building coreboot perspective, make or

30
00:01:09,200 --> 00:01:12,640
simply make all

31
00:01:10,320 --> 00:01:13,920
will start the building process of

32
00:01:12,640 --> 00:01:16,320
coreboot.

33
00:01:13,920 --> 00:01:17,680
Make clean removes the core but build

34
00:01:16,320 --> 00:01:19,840
artifacts,

35
00:01:17,680 --> 00:01:21,200
make this clean is a harder version of

36
00:01:19,840 --> 00:01:22,720
clean,

37
00:01:21,200 --> 00:01:25,680
besides the removal of the build

38
00:01:22,720 --> 00:01:27,280
artifacts it removes the config files

39
00:01:25,680 --> 00:01:30,240
from the payloads and other build

40
00:01:27,280 --> 00:01:33,360
results, it will simply leave the

41
00:01:30,240 --> 00:01:37,600
corporate repository clean from any

42
00:01:33,360 --> 00:01:39,680
result files. Make menuconfig,

43
00:01:37,600 --> 00:01:42,640
update the current configuration values

44
00:01:39,680 --> 00:01:44,880
utilizing a menu-based program,

45
00:01:42,640 --> 00:01:46,159
make alt config updates the current

46
00:01:44,880 --> 00:01:49,600
configuration file

47
00:01:46,159 --> 00:01:52,720
using a provided .config file as a base,

48
00:01:49,600 --> 00:01:55,759
make olddefconfig, is almost the same as

49
00:01:52,720 --> 00:01:58,399
oldconfig, but with default answer to

50
00:01:55,759 --> 00:02:01,200
all new options.

51
00:01:58,399 --> 00:02:01,840
Make devconfig creates new configuration

52
00:02:01,200 --> 00:02:04,960
file with

53
00:02:01,840 --> 00:02:06,159
default answer to all options, make

54
00:02:04,960 --> 00:02:08,000
safedevconfig,

55
00:02:06,159 --> 00:02:09,440
saves the current configuration file as

56
00:02:08,000 --> 00:02:11,920
devconfig,

57
00:02:09,440 --> 00:02:13,680
it creates somewhat minimal config

58
00:02:11,920 --> 00:02:17,120
because it skips all the

59
00:02:13,680 --> 00:02:19,520
default options.

60
00:02:17,120 --> 00:02:22,319
The reverse operation of the save dev

61
00:02:19,520 --> 00:02:24,959
config is all olddefconfig because it can

62
00:02:22,319 --> 00:02:25,680
recreate the full configuration file

63
00:02:24,959 --> 00:02:29,680
using the

64
00:02:25,680 --> 00:02:32,319
minimal config. Make crossgcc-ARCH

65
00:02:29,680 --> 00:02:34,319
it builds a cross compiler for

66
00:02:32,319 --> 00:02:37,840
a specific architecture,

67
00:02:34,319 --> 00:02:40,080
make crossgcc without passing ARCH

68
00:02:37,840 --> 00:02:42,000
will build cross compiler for all

69
00:02:40,080 --> 00:02:44,800
architectures.

70
00:02:42,000 --> 00:02:46,400
Make crosstools-ARCH builds the

71
00:02:44,800 --> 00:02:48,840
cross compiler with

72
00:02:46,400 --> 00:02:50,800
??? debugger for a specific

73
00:02:48,840 --> 00:02:55,120
architecture.

74
00:02:50,800 --> 00:02:58,400
Architecture can be x86 32-bit,

75
00:02:55,120 --> 00:03:02,840
or 64-bit, ARM 32-bit or

76
00:02:58,400 --> 00:03:06,879
ARM 64-bit, risk 5 power PC,

77
00:03:02,840 --> 00:03:08,239
etc. One may use also the CPUs parameter

78
00:03:06,879 --> 00:03:12,800
to build the toolchain using

79
00:03:08,239 --> 00:03:12,800
multiple cores, it may save some time.

80
00:03:13,680 --> 00:03:19,120
Despite coreboot offers an option to

81
00:03:17,120 --> 00:03:21,120
build the toolchain,

82
00:03:19,120 --> 00:03:22,959
building the cross toolchain is rather time

83
00:03:21,120 --> 00:03:24,959
consuming and may fail,

84
00:03:22,959 --> 00:03:27,120
it's rather recommended to use a Docker

85
00:03:24,959 --> 00:03:28,799
container for such case.

86
00:03:27,120 --> 00:03:30,400
However, if you have an exotic

87
00:03:28,799 --> 00:03:32,720
environment you may try to build the

88
00:03:30,400 --> 00:03:36,159
cross touching yourself or enable the

89
00:03:32,720 --> 00:03:39,280
and it will check gain config option.

90
00:03:36,159 --> 00:03:42,400
If you are feeling lucky, maybe you will

91
00:03:39,280 --> 00:03:45,440
get through it but do not count for any

92
00:03:42,400 --> 00:03:48,000
support in this case from the community.

93
00:03:45,440 --> 00:03:49,680
So, in order to avoid building the cross

94
00:03:48,000 --> 00:03:52,319
toolchain and save some time

95
00:03:49,680 --> 00:03:53,760
and possibly nerves, a Docker container

96
00:03:52,319 --> 00:03:55,840
is a recommended choice.

97
00:03:53,760 --> 00:03:57,360
We will not dig into why use Docker

98
00:03:55,840 --> 00:03:59,360
because it was already explained in

99
00:03:57,360 --> 00:04:02,319
previous chapters.

100
00:03:59,360 --> 00:04:04,159
So, let us just execute the command to

101
00:04:02,319 --> 00:04:06,720
switch into the bash inside the docker

102
00:04:04,159 --> 00:04:06,720
container,

103
00:04:11,760 --> 00:04:15,840
so this is the command that we will be

104
00:04:14,239 --> 00:04:17,919
typically using, to switch into the

105
00:04:15,840 --> 00:04:21,199
container.

106
00:04:17,919 --> 00:04:22,960
And now, we are inside the container

107
00:04:21,199 --> 00:04:26,080
and as we can see the coreboot

108
00:04:22,960 --> 00:04:29,520
directory of the source has been mounted

109
00:04:26,080 --> 00:04:33,280
inside the Docker container.

110
00:04:29,520 --> 00:04:40,639
Also, previous comments shall also work

111
00:04:33,280 --> 00:04:40,639
as we can see make club still works.


﻿1
00:00:00,044 --> 00:00:03,005
So, to sum up what we have learned so far

2
00:00:03,054 --> 00:00:08,041
the kconfig exposes the config options .config file

3
00:00:08,042 --> 00:00:11,005
to make files, which makes the build system our those

4
00:00:11,005 --> 00:00:16,017
settings. Based .config and config.h file

5
00:00:16,006 --> 00:00:19,004
the make file system can decide which is the target

6
00:00:19,004 --> 00:00:21,097
board, what's the size of the RAM and ROM,

7
00:00:22,001 --> 00:00:25,005
which source files should be built and assembled and in

8
00:00:25,005 --> 00:00:28,051
the code which versions should be built and which binaries

9
00:00:28,051 --> 00:00:32,005
doing plot in the final ROM image. In the previous

10
00:00:32,005 --> 00:00:32,058
lessons,

11
00:00:32,059 --> 00:00:35,083
I have mentioned that we have only one make file

12
00:00:35,084 --> 00:00:39,007
and together with the kconfig decides what sources are

13
00:00:39,007 --> 00:00:44,017
compiled in. coreboot has quite a vast source tree.

14
00:00:44,018 --> 00:00:48,006
So, how are those all other makefile.inc implies

15
00:00:48,061 --> 00:00:49,016
clouded.

16
00:00:49,084 --> 00:00:52,094
So, the main make file is looking for them in

17
00:00:52,094 --> 00:00:57,042
past that are written into the subdirs were able. At

18
00:00:57,042 --> 00:01:00,043
first this variable is defined as subdirs

19
00:01:00,043 --> 00:01:03,096
equal to ??? which points to the root directory

20
00:01:04,044 --> 00:01:06,066
There's only one makefile.inc file,

21
00:01:07,014 --> 00:01:09,085
we can call it main make falling file in the

22
00:01:09,085 --> 00:01:13,016
root directory at guests included by the make file.

23
00:01:15,014 --> 00:01:17,056
So, in the coreboot make fire system,

24
00:01:17,088 --> 00:01:21,055
the -y ??? are called classes,

25
00:01:22,004 --> 00:01:25,064
the main make fighting declares a variable subjects,

26
00:01:25,065 --> 00:01:26,043
-Y.

27
00:01:26,053 --> 00:01:28,065
Some of the past present in subdirs

28
00:01:28,065 --> 00:01:30,009
-Y

29
00:01:30,009 --> 00:01:34,006
class, defined in maine makefile.inc are presented on the slide.

30
00:01:34,073 --> 00:01:35,071
The other makefolder.inc

31
00:01:35,088 --> 00:01:40,018
files use the + = syntax to upend

32
00:01:40,019 --> 00:01:41,062
pass to the subdires

33
00:01:41,063 --> 00:01:45,048
-Y classes. When makefile.inc is included,

34
00:01:45,084 --> 00:01:49,077
the makefile extends the subdires variable by subdires

35
00:01:49,077 --> 00:01:51,045
-Y content from the cloud

36
00:01:51,046 --> 00:01:52,055
makefile.inc

37
00:01:53,004 --> 00:01:55,078
this means that path to look for makefolder.inc

38
00:01:55,078 --> 00:01:56,096
extends as well.

39
00:01:57,066 --> 00:02:01,007
Wild card is a function which takes a string with

40
00:02:01,008 --> 00:02:02,055
wild card as an argument.

41
00:02:03,000 --> 00:02:07,052
The output of this function is space separated list with names

42
00:02:07,052 --> 00:02:11,038
of existing files that matched to the pattern which was

43
00:02:11,038 --> 00:02:12,066
passed as an argument.

44
00:02:13,014 --> 00:02:13,097
For example,

45
00:02:14,029 --> 00:02:20,007
the result of $wildcard source/arch/src

46
00:02:20,008 --> 00:02:20,066


47
00:02:21,014 --> 00:02:24,089
is a space separated list of all files found in

48
00:02:24,009 --> 00:02:28,055
source/arc/directory.

49
00:02:30,014 --> 00:02:33,086
So, let's see how does it look like and code.

50
00:02:38,034 --> 00:02:38,055


51
00:02:46,094 --> 00:02:47,015


52
00:02:47,074 --> 00:02:47,095
Well,

53
00:02:49,024 --> 00:02:49,045


54
00:03:14,094 --> 00:03:15,015


55
00:03:16,014 --> 00:03:16,035


56
00:03:24,064 --> 00:03:24,096


57
00:03:42,014 --> 00:03:42,035


58
00:03:44,054 --> 00:03:44,075


59
00:03:45,034 --> 00:03:45,055


60
00:03:48,004 --> 00:03:48,024


61
00:03:49,034 --> 00:03:49,055


62
00:03:50,004 --> 00:03:50,054


63
00:03:54,074 --> 00:03:55,006


64
00:04:09,034 --> 00:04:09,054


65
00:04:10,094 --> 00:04:11,026


66
00:04:12,084 --> 00:04:13,005


67
00:04:13,044 --> 00:04:13,064


68
00:04:15,004 --> 00:04:15,054


69
00:04:21,074 --> 00:04:21,095


70
00:04:25,054 --> 00:04:25,075


71
00:04:28,034 --> 00:04:28,055


72
00:04:30,054 --> 00:04:30,075


73
00:04:32,084 --> 00:04:33,004


74
00:04:38,024 --> 00:04:38,045


75
00:04:43,094 --> 00:04:46,003
So, let's have a look at the main file

76
00:04:46,003 --> 00:04:47,075
in coreboot root directory.

77
00:04:48,024 --> 00:04:50,095
So, we have a veriable called the top level,

78
00:04:50,096 --> 00:04:53,055
which part the root directory of the registry.

79
00:04:54,024 --> 00:04:58,000
Then this total variable is used to include

80
00:04:58,000 --> 00:05:04,058
various makefile.incs like here, or to extend the subdires

81
00:05:04,058 --> 00:05:05,015
class.

82
00:05:06,044 --> 00:05:12,017
Then all the subdires classes evaluated to include other

83
00:05:12,017 --> 00:05:13,035
make files as well.

84
00:05:15,054 --> 00:05:17,036
And let's go to the makefile.inc

85
00:05:18,034 --> 00:05:21,017
The makefile.inc have another sub directories

86
00:05:21,017 --> 00:05:23,047
which are common for coreboot,

87
00:05:23,048 --> 00:05:25,021
they are automatically included

88
00:05:25,022 --> 00:05:25,078
for example,

89
00:05:25,078 --> 00:05:29,028
like src/lib, commonlib, console device and ACPI.

90
00:05:29,028 --> 00:05:29,076


91
00:05:30,074 --> 00:05:30,094


92
00:05:32,024 --> 00:05:32,088
And for example,

93
00:05:32,088 --> 00:05:36,034
if we go to one of the directories mentioned here,

94
00:05:36,035 --> 00:05:38,026
let's say srclib.

95
00:05:38,074 --> 00:05:38,094


96
00:05:39,074 --> 00:05:43,016
We'll find a corresponding makefile.inc there too.

97
00:05:44,024 --> 00:05:45,081
Let's see we are a sourcelib

98
00:05:45,082 --> 00:05:51,015
makefile.inc now. And currently we shall not

99
00:05:51,064 --> 00:05:58,046
any the subdirectories except some gnat subdirectory.

100
00:05:59,004 --> 00:06:03,067
So, if there is no other subdirectories typically, makefile.inc

101
00:06:03,067 --> 00:06:07,092
indicates which source file should be compiled

102
00:06:07,092 --> 00:06:10,006
into given program.

103
00:06:10,054 --> 00:06:14,031
So, we have also another classes like ROM stage,

104
00:06:14,031 --> 00:06:15,097
and RAM stage, the compressor,

105
00:06:16,013 --> 00:06:16,076
pull block,

106
00:06:17,024 --> 00:06:20,042
and we see that appends on source files to these

107
00:06:20,042 --> 00:06:20,086
classes.

108
00:06:21,034 --> 00:06:21,055


109
00:06:25,084 --> 00:06:28,055
So, as I have mentioned earlier under makefile.inc

110
00:06:28,056 --> 00:06:31,071
files and deeper in the sauce tree may extend

111
00:06:31,071 --> 00:06:32,077
the subjects variables as

112
00:06:32,077 --> 00:06:33,039
well.

113
00:06:33,004 --> 00:06:36,055
This is the recursive process of including makefile.inc file

114
00:06:36,056 --> 00:06:40,068
extending search path including another file

115
00:06:40,069 --> 00:06:42,046
and subdires ??? become empty.

116
00:06:43,004 --> 00:06:44,045
So to sum it up,

117
00:06:45,004 --> 00:06:47,046
we start with including the main makefile.inc from

118
00:06:47,046 --> 00:06:51,041
root directory, and then the subject is extended by subs

119
00:06:51,041 --> 00:06:52,053
-Y from main

120
00:06:52,053 --> 00:06:53,046
makefile.inc

121
00:06:54,004 --> 00:06:54,076
It contains

122
00:06:54,077 --> 00:06:58,063
paths to other core directories which should be research for

123
00:06:58,063 --> 00:07:00,016
makefile.inc files.

124
00:07:00,064 --> 00:07:04,065
One of the paths is subtracted from subjects variable and

125
00:07:04,065 --> 00:07:08,079
we search for makefile.inc there, if it exists it

126
00:07:08,079 --> 00:07:12,092
gets included, if it contains subdires -Y, it's further

127
00:07:12,092 --> 00:07:14,086
extends the subdires variable.

128
00:07:15,084 --> 00:07:19,076
The process finishes when the subdires variable becomes empty.

129
00:07:21,004 --> 00:07:24,002
So, as I have mentioned the previous lessons,

130
00:07:24,001 --> 00:07:27,046
there are also other classes defined the makefile.inc files.

131
00:07:27,091 --> 00:07:30,079
The most important could be the RAM stage ROM stage,

132
00:07:30,079 --> 00:07:34,078
and root block. Their names correspond to coreboot stages

133
00:07:34,078 --> 00:07:36,015
to which they are related.

134
00:07:37,004 --> 00:07:40,009
So, a short reminder about the current status from previous

135
00:07:40,009 --> 00:07:40,056
chapter.

136
00:07:41,014 --> 00:07:44,059
So, the boot block class uh contains the service files

137
00:07:44,006 --> 00:07:48,036
that consists of the boot block stage.

138
00:07:48,083 --> 00:07:52,033
The boot block stages contains the recent victor and the caches

139
00:07:52,033 --> 00:07:53,015
RAM set up.

140
00:07:53,084 --> 00:07:57,079
The RAM stage is responsible for at least reconciliation at

141
00:07:57,079 --> 00:07:58,066
memory setup.

142
00:07:59,044 --> 00:08:00,084
Postcard tears

143
00:08:00,084 --> 00:08:05,067
dedication some environment down, RAM stage performs normal device setup

144
00:08:05,077 --> 00:08:09,026
and main board configuration and the last stages payload.

145
00:08:09,064 --> 00:08:12,095
Which is typically the operating system or application boot loader.

146
00:08:14,035 --> 00:08:16,094
They may founded in files laying deeper in the source tree.

147
00:08:17,036 --> 00:08:17,092
For example,

148
00:08:17,092 --> 00:08:20,009
in the main world directory usually add some files that

149
00:08:20,009 --> 00:08:22,072
should be built to those classes.

150
00:08:23,021 --> 00:08:26,004
In this example we instruct the build system to build

151
00:08:26,004 --> 00:08:29,022
??? and IRQU as a part of

152
00:08:29,022 --> 00:08:29,095
RAM stage block.

153
00:08:31,024 --> 00:08:32,099
After the build process finishes,

154
00:08:33,000 --> 00:08:35,078
you can see the output files are also split by

155
00:08:35,078 --> 00:08:36,056
a class.

156
00:08:36,057 --> 00:08:39,005
There are also directories such as build RAM stage,

157
00:08:39,005 --> 00:08:41,095
build ROM stage, build boot lock, etc.

158
00:08:42,044 --> 00:08:45,087
Which contained the compiled object files that were privacy appended

159
00:08:46,031 --> 00:08:47,045
to the given class.

160
00:08:49,014 --> 00:08:51,026
So, let's have a look at an example

161
00:08:51,084 --> 00:08:52,025


162
00:08:52,026 --> 00:08:59,015
makefile.inc files that include the source files.

163
00:08:59,074 --> 00:09:01,004
So for example,

164
00:09:01,005 --> 00:09:04,096
as a purposely showing you the sources makefile.inc file

165
00:09:05,034 --> 00:09:08,077
you can see that there are certain libraries and clubs

166
00:09:08,077 --> 00:09:09,076
in the boot block.

167
00:09:10,092 --> 00:09:11,065
For example,

168
00:09:11,065 --> 00:09:12,066
for the first stage,

169
00:09:12,067 --> 00:09:16,035
which is used to we would-we have a separate

170
00:09:16,006 --> 00:09:21,036
in class for a ROM stage class and for the

171
00:09:21,036 --> 00:09:22,039
RAM stage as well,

172
00:09:22,004 --> 00:09:27,006
we see some specific sources that are compiled

173
00:09:27,006 --> 00:09:28,036
to the RAM stage.

174
00:09:29,034 --> 00:09:30,025
Additionally,

175
00:09:30,025 --> 00:09:34,002
you can see that certain source files can be compiled

176
00:09:34,002 --> 00:09:36,015
dependent on the kconfig options,

177
00:09:36,054 --> 00:09:40,078
so this variable is included from the kconfig

178
00:09:40,078 --> 00:09:45,069
file, result file and ??? to Y,

179
00:09:45,007 --> 00:09:48,085
which expands the RAM stage - to the ROM stage

180
00:09:48,085 --> 00:09:49,046
-Y.

181
00:09:50,004 --> 00:09:53,075
That means the cbmem console is compared to RAM stage

182
00:09:53,075 --> 00:09:59,003
only if the config council cbmem is selected to a

183
00:09:59,003 --> 00:10:01,046
positive value, because it is a bullying.

184
00:10:06,084 --> 00:10:10,003
So, to show another point of connection between the makefile.inc

185
00:10:10,003 --> 00:10:11,025
config,

186
00:10:11,074 --> 00:10:13,093
let's have a look at example,

187
00:10:13,094 --> 00:10:18,056
kconfig from emulation board for QEMU and

188
00:10:18,056 --> 00:10:19,075
chipset q35.

189
00:10:20,034 --> 00:10:23,006
So, I'm picking our mainboard in menu config,

190
00:10:23,064 --> 00:10:26,082
it doesn't end up with just one config selection being

191
00:10:26,083 --> 00:10:27,036
changed.

192
00:10:27,074 --> 00:10:29,092
If you take a look at the corresponding kconfig

193
00:10:29,092 --> 00:10:30,006
file,

194
00:10:30,061 --> 00:10:35,055
you can see a set of configuration options and the

195
00:10:35,056 --> 00:10:38,044
values that are selected after this particular mainboard has

196
00:10:38,044 --> 00:10:41,069
been picked, and those selected by the mainboard kconfig

197
00:10:41,069 --> 00:10:45,034
select other dependent options throughout the whole coreboot source.

198
00:10:46,044 --> 00:10:46,095


199
00:10:48,004 --> 00:10:52,071
It is simplified because one does not simply remember what

200
00:10:52,071 --> 00:10:55,061
options should be set for a given mainboard.

201
00:10:56,014 --> 00:10:59,012
So, the relation chain others to simplify things on the

202
00:10:59,012 --> 00:11:02,007
main board level while keep the complex things in

203
00:11:02,007 --> 00:11:04,064
the kconfigs,

204
00:11:04,065 --> 00:11:05,029
for example,

205
00:11:05,029 --> 00:11:08,032
for the associate, for the north bridge or the salvage

206
00:11:08,032 --> 00:11:09,045
for given platform.

207
00:11:11,054 --> 00:11:15,056
So, on another example of makefile concussion connection,

208
00:11:15,057 --> 00:11:20,036
maybe the inclusion of the mainboard specific makefile.inc.

209
00:11:21,034 --> 00:11:22,048
The previous slides,

210
00:11:22,049 --> 00:11:24,003
I have shown the subject

211
00:11:24,003 --> 00:11:27,061
that's why declaration and make in the main makefile.inc

212
00:11:27,061 --> 00:11:31,058
file. Makefile.inc at source

213
00:11:31,058 --> 00:11:35,086
Main board and variable main board duties subdirs -Y.

214
00:11:36,054 --> 00:11:39,005
You can also see that the mainboard variable comes

215
00:11:39,005 --> 00:11:43,017
from the config mainboard which is selected by

216
00:11:43,017 --> 00:11:47,012
kconfig when we set them the given mainboard.

217
00:11:47,027 --> 00:11:47,005
Now,

218
00:11:47,005 --> 00:11:48,012
for example,

219
00:11:48,012 --> 00:11:49,036
to some relation QEMU

220
00:11:49,036 --> 00:11:50,046
q35.

221
00:11:51,004 --> 00:11:54,029
This makes that only mainboard directory for this particular

222
00:11:54,029 --> 00:11:57,055
board is added to the subdirectory variable.

223
00:11:59,034 --> 00:12:01,055
So, let's see how it looks in the code.

224
00:12:05,084 --> 00:12:11,049
Let's go to the mainboard directory in emulation and

225
00:12:11,049 --> 00:12:12,096
QEMU-q35.

226
00:12:14,064 --> 00:12:20,085
So, you can se that there are various kconfig

227
00:12:22,004 --> 00:12:26,075
conditions like have both emulation QEMU X 86 q35.

228
00:12:27,014 --> 00:12:31,079
This option comes from kconfig.name,

229
00:12:31,008 --> 00:12:32,036
for example,

230
00:12:33,014 --> 00:12:38,044
which is automatically included by the kconfig on the

231
00:12:38,044 --> 00:12:39,026
higher level.

232
00:12:39,027 --> 00:12:44,057
So, if we have the emulation vendor we have

233
00:12:44,063 --> 00:12:48,053
kconfig and kconfig.name for

234
00:12:48,054 --> 00:12:50,076
each vendor in coreboot.

235
00:12:51,054 --> 00:12:56,062
So, we define the vendor and main board model and

236
00:12:56,062 --> 00:12:59,008
as well we source the kconfig.name and that

237
00:12:59,081 --> 00:13:02,085
config files from each main board model.

238
00:13:05,003 --> 00:13:08,097
This way, we only have more specific options for a

239
00:13:08,097 --> 00:13:10,096
single main board we select.

240
00:13:12,034 --> 00:13:13,005
So for example,

241
00:13:14,000 --> 00:13:19,002
let's see our emulation boards for QEMU SOUTHBRIDGE

242
00:13:19,002 --> 00:13:23,035
INTEL and this model ???.

243
00:13:23,094 --> 00:13:27,054
So, it already tells us where to look for the

244
00:13:27,054 --> 00:13:30,026
kconfig which is responsible for defining this option.

245
00:13:31,004 --> 00:13:32,095
We may look for the source,

246
00:13:32,096 --> 00:13:40,016
southbridge, intel and here we have, it's this one.

247
00:13:42,061 --> 00:13:47,006
And as we can see the more complex options which

248
00:13:47,044 --> 00:13:50,057
should be a main board diagnostic are included here.

249
00:13:50,073 --> 00:13:52,096
There are ??? related to the southbridge.

250
00:13:53,064 --> 00:13:58,048
So, we don't have to know which options are specific

251
00:13:58,048 --> 00:13:59,018
to southbridge.

252
00:13:59,018 --> 00:14:02,029
We just said the southbridge the mainboard is

253
00:14:02,029 --> 00:14:02,076
using.

254
00:14:05,014 --> 00:14:07,002
And so we have other options,

255
00:14:07,002 --> 00:14:14,016
like some CMOS configuration, we have some BOARD ROM size defined

256
00:14:15,029 --> 00:14:23,016
and other stuff. Besides the heart selection of

257
00:14:23,054 --> 00:14:25,066
the board specific options,

258
00:14:26,004 --> 00:14:31,098
we can define certain defaults which are selected when given

259
00:14:31,099 --> 00:14:32,092
option is enabled.

260
00:14:32,092 --> 00:14:35,087
So, if you enable vboot for example, we can

261
00:14:35,087 --> 00:14:38,093
select the options that are specific for this board as

262
00:14:38,093 --> 00:14:39,036
well.

263
00:14:44,046 --> 00:14:47,058
And here we also can see that the mainboard DIR

264
00:14:47,058 --> 00:14:48,056
is set here,

265
00:14:49,084 --> 00:14:56,045
this mainboard DIR contains the path or rather the

266
00:14:56,084 --> 00:14:59,095
part of the pen that is used to construct various

267
00:14:59,095 --> 00:15:01,076
other paths in the boot system.

268
00:15:02,092 --> 00:15:05,068
As we can see there are many references across

269
00:15:05,068 --> 00:15:08,054
the across ???.

270
00:15:10,014 --> 00:15:11,076
Going back to the makefiles,

271
00:15:12,014 --> 00:15:15,006
we can see that the mainbor is avoided here.

272
00:15:19,011 --> 00:15:24,098
And it's just all over the the source tree and

273
00:15:24,098 --> 00:15:27,065
this is including of course as a subdirectory as well.

274
00:15:28,024 --> 00:15:28,045


275
00:15:31,014 --> 00:15:33,027
So, if we take a look back,

276
00:15:33,028 --> 00:15:36,041
we will see that the main makefile.inc add all

277
00:15:36,042 --> 00:15:38,006
??? northbridges,

278
00:15:38,006 --> 00:15:39,003
southbridges,

279
00:15:39,004 --> 00:15:41,072
etc directories to the

280
00:15:41,073 --> 00:15:42,009
subdires

281
00:15:42,009 --> 00:15:43,005
-Y.

282
00:15:43,055 --> 00:15:45,056
Regardless of the chosen platform,

283
00:15:46,004 --> 00:15:49,022
but if you open one of these makefile.inc

284
00:15:49,023 --> 00:15:51,013
files, from those directories,

285
00:15:51,015 --> 00:15:54,027
we will see that there is an appropriate if equal

286
00:15:54,027 --> 00:15:56,005
check at the beginning of the file.

287
00:15:56,084 --> 00:16:01,025
This equal check ensures that only this content

288
00:16:01,025 --> 00:16:03,091
is only exposed to the main makefile

289
00:16:03,092 --> 00:16:07,037
if the corresponding config option has been selected ??? during

290
00:16:07,037 --> 00:16:08,086
the build configuration phase.

291
00:16:10,004 --> 00:16:10,079
In such way,

292
00:16:10,079 --> 00:16:13,095
only the system on chip southbridge or northbridge specific

293
00:16:13,095 --> 00:16:16,014
code and options are included.

294
00:16:16,036 --> 00:16:17,036
In our case,

295
00:16:17,036 --> 00:16:18,085
it will be the intel server

296
00:16:18,085 --> 00:16:20,096
which I have mentioned in the previous lesson.

297
00:16:21,074 --> 00:16:24,006
So, let's have a look how they makefile.inc

298
00:16:24,006 --> 00:16:27,005
file looks in for this southbridge.

299
00:16:29,024 --> 00:16:29,045


300
00:16:30,024 --> 00:16:34,089
So, opening the makefile.inc from the southbridge

301
00:16:34,092 --> 00:16:40,007
and we see this is the feq conditions that

302
00:16:40,008 --> 00:16:44,077
ensures only the content of this make part is included

303
00:16:45,054 --> 00:16:47,007
when the southbridges enabled.

304
00:16:47,094 --> 00:16:51,098
So, we have the corresponding sources here,

305
00:16:51,099 --> 00:16:54,036
which are included for the specific southbridge.


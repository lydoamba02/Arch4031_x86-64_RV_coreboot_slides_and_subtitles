﻿1
00:00:00,054 --> 00:00:03,047
So, we already know how the coreboot build system

2
00:00:03,047 --> 00:00:07,084
looks like and what is built during the coreboot build

3
00:00:07,084 --> 00:00:08,046
process.

4
00:00:09,004 --> 00:00:10,000
However,

5
00:00:10,003 --> 00:00:13,096
we don't know yet what exactly is building our coreboot.

6
00:00:14,064 --> 00:00:18,065
So, coreboot toolchain is necessary to build the

7
00:00:18,065 --> 00:00:19,095
coreboot binary image.

8
00:00:20,044 --> 00:00:25,051
These toolchain consists of compilers, ??? utilities package and

9
00:00:25,051 --> 00:00:28,066
??? deathlibs and ACPI compiler.

10
00:00:29,004 --> 00:00:34,035
So, basically the compiler is compiling colde and binuntils

11
00:00:34,035 --> 00:00:40,076
contains the linker which links the whole compiled objects

12
00:00:40,077 --> 00:00:46,068
by GCC into a single executable. Libncurses are

13
00:00:46,068 --> 00:00:53,069
mainly used to display the configuration many which

14
00:00:53,069 --> 00:00:55,031
is used to create the text

15
00:00:55,031 --> 00:00:58,046
UI right and is also used in the lib payload.

16
00:00:59,044 --> 00:01:04,031
The ACPI source code compiler is used to compile

17
00:01:04,032 --> 00:01:08,061
ACPI tables and code which is included

18
00:01:08,061 --> 00:01:10,006
in the coreboot source tree.

19
00:01:11,034 --> 00:01:13,046
Why do we even need the toolchain?

20
00:01:14,033 --> 00:01:14,078
Well,

21
00:01:14,079 --> 00:01:17,026
building coreboot image is one thing,

22
00:01:17,084 --> 00:01:23,026
but one should note that coreboot supports multiple architectures. So

23
00:01:23,027 --> 00:01:26,066
we need a different toolchain for each architecture.

24
00:01:27,004 --> 00:01:30,075
So, if you want to build for given target architecture,

25
00:01:30,076 --> 00:01:32,095
cost compilation maybe often necessary.

26
00:01:33,054 --> 00:01:37,011
We also like to avoid incompatibility issues due to some

27
00:01:37,011 --> 00:01:38,016
compiler set up.

28
00:01:39,004 --> 00:01:41,099
Also compiler ships with many distributions may not be

29
00:01:41,099 --> 00:01:44,005
compatible to perform a coreboot build.

30
00:01:44,044 --> 00:01:45,046
We all use the same,

31
00:01:45,046 --> 00:01:47,001
build environment that so

32
00:01:47,001 --> 00:01:49,036
it should be easier to set up and resolve potential

33
00:01:49,036 --> 00:01:49,096
issues.

34
00:01:52,004 --> 00:01:55,057
So, how to build the coreboot toolchain, in order

35
00:01:55,057 --> 00:01:57,077
to build all available tool chains,

36
00:01:57,078 --> 00:01:58,063
we can use them,

37
00:01:58,063 --> 00:02:02,006
make across gcc command to build all in one shot.

38
00:02:02,074 --> 00:02:05,026
You can also build a specific toolchain,

39
00:02:05,084 --> 00:02:10,066
we have to just specify the target architecture and

40
00:02:10,066 --> 00:02:12,079
make cross gcc

41
00:02:12,079 --> 00:02:13,096
-ARC.

42
00:02:14,094 --> 00:02:18,026
So, usually it is enough to build this one since.

43
00:02:18,074 --> 00:02:22,063
we're mainly going to be building for example for x86.

44
00:02:22,063 --> 00:02:23,015


45
00:02:24,024 --> 00:02:27,078
So, I would not recommend building and the coreboot toolchain

46
00:02:27,078 --> 00:02:30,098
because it is very time consuming and this of

47
00:02:30,098 --> 00:02:31,095
an error prone.

48
00:02:33,004 --> 00:02:38,027
We recommend to use the Docker container. If you decide

49
00:02:38,028 --> 00:02:41,056
to build the toolchain manually, after the build process,

50
00:02:41,057 --> 00:02:44,094
it can be found at the audience cross gcc/xgcc/bin

51
00:02:44,095 --> 00:02:47,006
directory.

52
00:02:47,074 --> 00:02:50,045
You will see that some of the tools that are

53
00:02:50,045 --> 00:02:51,088
part of the toolchain on the slide,

54
00:02:51,088 --> 00:02:55,037
but in fact the list is much longer. Inside the

55
00:02:55,037 --> 00:02:55,094
Docker,

56
00:02:55,095 --> 00:02:59,037
you might find it in opt xgcc being, there will

57
00:02:59,037 --> 00:03:01,095
be lots of files for various architectures.

58
00:03:03,004 --> 00:03:06,037
So, let's check out how does it look like, I'm

59
00:03:06,042 --> 00:03:08,056
inside the Doctor already.

60
00:03:09,004 --> 00:03:11,095
So, let's check how it looks like,

61
00:03:17,025 --> 00:03:21,001
as you can see there is a toolchain for 64-bit

62
00:03:21,001 --> 00:03:23,016
architecture on x86.

63
00:03:23,054 --> 00:03:31,032
Have 64 bit risk five, 64-bit or powerpc and

64
00:03:31,032 --> 00:03:35,007
nds vm, and 32-bit x86 architecture.

65
00:03:37,014 --> 00:03:37,034


66
00:03:37,084 --> 00:03:46,054
Also few lines that are 64-bit. So,

67
00:03:46,055 --> 00:03:48,015
as I have mentioned here,

68
00:03:48,016 --> 00:03:51,035
the cross compilation of coorboot toolchain is often necessary.

69
00:03:52,014 --> 00:03:56,025
But, how to tell corporate where our toolchain is, and

70
00:03:56,025 --> 00:03:59,036
how to use it. An important part,

71
00:03:59,094 --> 00:04:03,009
this process takes the xcompile file which lays in

72
00:04:03,009 --> 00:04:05,036
the corporate directory.

73
00:04:05,091 --> 00:04:09,001
It contains paths to toolchain binaries, as well as

74
00:04:09,001 --> 00:04:10,001
compilation flags

75
00:04:10,001 --> 00:04:11,099
which are required in the build process.

76
00:04:12,054 --> 00:04:15,033
It is included in the makefile to set variables,

77
00:04:15,033 --> 00:04:16,043
like CC,

78
00:04:16,043 --> 00:04:16,085


79
00:04:16,086 --> 00:04:17,001


80
00:04:17,001 --> 00:04:17,076
C flags,

81
00:04:17,077 --> 00:04:18,053
CPP,

82
00:04:18,054 --> 00:04:19,005
object

83
00:04:19,005 --> 00:04:19,055
copy,

84
00:04:19,056 --> 00:04:20,002
object

85
00:04:20,003 --> 00:04:20,041
dump,

86
00:04:20,042 --> 00:04:21,015
etc.

87
00:04:21,094 --> 00:04:23,076
Let's see how it looks in the code.

88
00:04:28,034 --> 00:04:30,034
So, here we have the target,

89
00:04:30,034 --> 00:04:34,072
which is the .xcompilefile which is built

90
00:04:34,073 --> 00:04:38,004
with you to xcompile, xcompile. As we can

91
00:04:38,004 --> 00:04:38,084
see it

92
00:04:38,085 --> 00:04:41,015
??? xgccpatch,

93
00:04:41,084 --> 00:04:44,015
but how the xgcc is set.

94
00:04:47,034 --> 00:04:51,047
It is used the xcompile file and with the

95
00:04:51,047 --> 00:05:00,076
xcompile utility to produce it, and another usage is

96
00:05:01,014 --> 00:05:01,046


97
00:05:02,084 --> 00:05:05,013
in the xgccpath,

98
00:05:06,004 --> 00:05:11,076
it checks the default paths for the cross toolchain and

99
00:05:11,076 --> 00:05:13,042
it said the environment variable,

100
00:05:13,055 --> 00:05:15,086
which is then consumed by the makefile.


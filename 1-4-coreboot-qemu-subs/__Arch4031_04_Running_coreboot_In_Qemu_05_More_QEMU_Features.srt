﻿1
00:00:00,024 --> 00:00:01,005
As I said previously,

2
00:00:01,005 --> 00:00:05,001
QEMU provides quite a rich ecosystem,

3
00:00:05,001 --> 00:00:09,003
it has many features. Definitely, you can easily find

4
00:00:09,031 --> 00:00:12,086
documentation which describes all these interesting features.

5
00:00:13,024 --> 00:00:18,012
One of the most important one that it's

6
00:00:18,013 --> 00:00:22,003
good to use during the former development is GDB

7
00:00:22,003 --> 00:00:26,000
hooking with GDB to QEMU

8
00:00:26,000 --> 00:00:32,045
and debug whatever is running inside. Some execution locks

9
00:00:32,046 --> 00:00:34,029
some dumping of the

10
00:00:34,003 --> 00:00:37,016
what's going on inside the emulated environment.

11
00:00:37,054 --> 00:00:39,023
We can precisely see,

12
00:00:39,024 --> 00:00:40,023
for example,

13
00:00:40,023 --> 00:00:44,087
from what locations code that is running inside

14
00:00:44,087 --> 00:00:47,047
QEMU reads where it writes.

15
00:00:47,048 --> 00:00:51,046
I know there are there is some work related

16
00:00:51,046 --> 00:00:56,029
to management engine emulation and then you can of

17
00:00:56,029 --> 00:00:59,005
course they do some exploration of the systems,

18
00:00:59,014 --> 00:01:01,026
if you are interested in that part.

19
00:01:01,078 --> 00:01:03,059
There are many other ways,

20
00:01:03,059 --> 00:01:06,087
there are some extensions to QEMU

21
00:01:06,088 --> 00:01:07,053
for example,

22
00:01:07,053 --> 00:01:11,026
to support the RTM to support SGX.

23
00:01:11,034 --> 00:01:13,006
So,

24
00:01:13,054 --> 00:01:14,043
QEMU is

25
00:01:14,043 --> 00:01:16,088
very rich and can be very helpful during the firmware

26
00:01:16,088 --> 00:01:17,065
development.

27
00:01:18,044 --> 00:01:20,045
Of course,

28
00:01:20,045 --> 00:01:22,007
like how typical you will use that,

29
00:01:22,071 --> 00:01:26,091
it has good potential to do shift left strategy.

30
00:01:26,091 --> 00:01:29,057
So, you don't have hardware or you are limited with

31
00:01:29,057 --> 00:01:33,045
supporting capabilities, or you simply just developing some payload and

32
00:01:33,045 --> 00:01:37,047
you don't have to do that on the

33
00:01:37,047 --> 00:01:38,025
hardware.

34
00:01:38,026 --> 00:01:42,091
And that's good, because like you can speed up develop,

35
00:01:42,091 --> 00:01:43,037
test,

36
00:01:43,037 --> 00:01:47,072
fix, cycle and just basing on QEMU. Of course, this

37
00:01:47,072 --> 00:01:52,097
means some level of abstraction and then you have

38
00:01:52,097 --> 00:01:55,008
to understand what's the difference between what you have in

39
00:01:55,008 --> 00:01:58,065
QEMU and what you have in real hardware.

40
00:01:59,014 --> 00:02:01,035
Other things which are very important

41
00:02:01,036 --> 00:02:05,097
are implementations of um support for risk five and open

42
00:02:05,097 --> 00:02:09,093
power in QEMU. Thanks to that port in coreboot

43
00:02:09,093 --> 00:02:12,056
was quite easy.

44
00:02:12,057 --> 00:02:13,007


45
00:02:13,071 --> 00:02:16,096
At least there were tools for that and

46
00:02:16,096 --> 00:02:21,006
that definitely speed up growth of ecosystem of alternative

47
00:02:21,006 --> 00:02:22,022
architectures

48
00:02:22,022 --> 00:02:24,004
which would bring additional value

49
00:02:24,041 --> 00:02:25,016


50
00:02:25,024 --> 00:02:26,066
in area of openness

51
00:02:26,067 --> 00:02:28,006
in area of security,

52
00:02:28,044 --> 00:02:32,068
which should be of interest to any coreboot fan,

53
00:02:32,069 --> 00:02:33,096
coreboot enthusiast.

54
00:02:35,024 --> 00:02:39,005
So, let's go to the quizzes and assignment.


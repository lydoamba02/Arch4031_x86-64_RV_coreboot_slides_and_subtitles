﻿1
00:00:00,044 --> 00:00:02,073
Let's talk a little bit about CBMEM.

2
00:00:02,074 --> 00:00:06,056
CBMEM is a special region in the RAM reserved for

3
00:00:06,057 --> 00:00:07,000
logs,

4
00:00:07,001 --> 00:00:12,015
performance, measuring and various other information that korbut storing during

5
00:00:12,015 --> 00:00:12,086
the boot time.

6
00:00:13,044 --> 00:00:16,093
The source code for the tool that extract that information

7
00:00:16,093 --> 00:00:22,033
from the memory is in coreboot source tree and location of

8
00:00:22,033 --> 00:00:23,001
you utils/cbmem.

9
00:00:23,002 --> 00:00:23,076


10
00:00:24,074 --> 00:00:25,054


11
00:00:25,055 --> 00:00:30,084
It can be those information which exposed CBMEM tool, it can

12
00:00:30,084 --> 00:00:33,068
be also access it through the payload which is used

13
00:00:33,068 --> 00:00:36,028
during the payload development.

14
00:00:36,029 --> 00:00:40,028
You usually payload for some various useful features like

15
00:00:40,029 --> 00:00:43,046
getting those information from memory.

16
00:00:44,054 --> 00:00:48,036
You may think that such kind of logging has

17
00:00:48,036 --> 00:00:52,011
some overhead, but it is developed in a way that

18
00:00:52,011 --> 00:00:53,056
there is almost no overhead.

19
00:00:53,094 --> 00:00:59,083
Unfortunately, there is static allocations, statically allocated space

20
00:00:59,083 --> 00:01:01,014
for the logs.

21
00:01:01,015 --> 00:01:04,071
So, if you have logs which are two variables,

22
00:01:04,072 --> 00:01:08,009
you can easily run out of space, in that

23
00:01:08,001 --> 00:01:12,025
case some part of logs is truncated and you're just

24
00:01:12,026 --> 00:01:16,018
losing that but typically you don't need everything,

25
00:01:16,019 --> 00:01:18,056
you just need some interesting part.

26
00:01:19,044 --> 00:01:22,056
And of course, you can decrease the ???.

27
00:01:24,004 --> 00:01:26,086
In some cases, it may be also very hard to

28
00:01:27,024 --> 00:01:28,075
access those information,

29
00:01:28,075 --> 00:01:30,096
especially if you can not put the platform.

30
00:01:31,034 --> 00:01:33,035
This happened a lot during the

31
00:01:34,004 --> 00:01:37,076
bring up process, during the initial porting of coreboot.

32
00:01:38,034 --> 00:01:40,095
In that case you may

33
00:01:40,096 --> 00:01:42,055
use JTAG.

34
00:01:42,056 --> 00:01:46,007
There are also other methods which we probably

35
00:01:46,008 --> 00:01:50,042
will explain in four the trainings.

36
00:01:50,043 --> 00:01:51,005


37
00:01:51,005 --> 00:01:52,091
Despite its limitation,

38
00:01:52,092 --> 00:01:58,057
CBMEM is a standard tool for gathering the information

39
00:01:58,058 --> 00:02:01,061
especially, if you don't have serial output,

40
00:02:01,061 --> 00:02:06,015
which can give you the same information or similar information.

41
00:02:06,084 --> 00:02:07,012
So,

42
00:02:07,012 --> 00:02:08,037
to compile the tool,

43
00:02:08,038 --> 00:02:15,081
you're just going to utils/cbmem make and let's

44
00:02:15,081 --> 00:02:16,085
try to do that.

45
00:02:18,054 --> 00:02:19,016


46
00:02:22,054 --> 00:02:33,045


47
00:02:33,045 --> 00:02:35,018
You can see we have CBMEM

48
00:02:35,019 --> 00:02:36,029
it is very,

49
00:02:36,029 --> 00:02:37,024
very small,

50
00:02:37,025 --> 00:02:40,036
and you can use it.


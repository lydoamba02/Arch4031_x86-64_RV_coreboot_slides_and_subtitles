﻿1
00:00:00,080 --> 00:00:04,639
We will use QEMU system i386,

2
00:00:06,879 --> 00:00:14,000
and at the end we will add

3
00:00:10,880 --> 00:00:17,199
-s -S.

4
00:00:14,000 --> 00:00:17,520
So, as you can see nothing's booting QEMU

5
00:00:17,199 --> 00:00:20,800
just

6
00:00:17,520 --> 00:00:24,480
waiting for hooking into GDB server.

7
00:00:20,800 --> 00:00:24,480
So, let's open other terminal,

8
00:00:26,720 --> 00:00:36,000
let's find ID of our container,

9
00:00:31,840 --> 00:00:36,000
let's run brush in it.

10
00:00:37,200 --> 00:00:45,840
Okay, and now we should be able

11
00:00:40,640 --> 00:00:45,840
to GDP,

12
00:00:51,039 --> 00:00:55,840
of course we have to go to correct

13
00:00:53,600 --> 00:01:00,160
directory.

14
00:00:55,840 --> 00:01:04,559
Build CBFS fallback,

15
00:01:00,160 --> 00:01:04,559
and now we can run GDB.

16
00:01:11,200 --> 00:01:16,479
Okay, we have GDB missing. So, let's

17
00:01:13,840 --> 00:01:16,479
install it.

18
00:01:23,280 --> 00:01:27,840
It should be quite fast,

19
00:01:34,479 --> 00:01:42,320
okay we're in and you can see that

20
00:01:38,560 --> 00:01:44,720
the boot block reading sables were

21
00:01:42,320 --> 00:01:48,320
already found.

22
00:01:44,720 --> 00:01:55,759
So, we are sure we are inside

23
00:01:48,320 --> 00:01:58,240
and we have correctly loaded symbols.

24
00:01:55,759 --> 00:01:59,439
Unfortunately, there are some limitations

25
00:01:58,240 --> 00:02:03,560
of GDB,

26
00:01:59,439 --> 00:02:07,680
despite we will use set arch

27
00:02:03,560 --> 00:02:08,160
i8086 GDB, still cannot correctly debug

28
00:02:07,680 --> 00:02:09,280


29
00:02:08,160 --> 00:02:13,520
[Music]

30
00:02:09,280 --> 00:02:13,520
16-bit code at ??? vector.

31
00:02:13,680 --> 00:02:20,879
But, we have

32
00:02:17,280 --> 00:02:22,959
access to all the symbols of boot block.

33
00:02:20,879 --> 00:02:23,920
So, all the code inside the boot block

34
00:02:22,959 --> 00:02:26,640
can be debugged

35
00:02:23,920 --> 00:02:28,560
without the problems. We can add other

36
00:02:26,640 --> 00:02:32,000
stages of the

37
00:02:28,560 --> 00:02:35,920
boot process like ROM stage RAM stage,

38
00:02:32,000 --> 00:02:37,040
postcard by using simple file name

39
00:02:35,920 --> 00:02:39,280
and

40
00:02:37,040 --> 00:02:40,319
add symbol file file name and providing

41
00:02:39,280 --> 00:02:43,760
the address.

42
00:02:40,319 --> 00:02:44,239
But, there is some small coverage symbols

43
00:02:43,760 --> 00:02:46,560
in

44
00:02:44,239 --> 00:02:48,840
debug files for next stages use

45
00:02:46,560 --> 00:02:51,599
different base address.

46
00:02:48,840 --> 00:02:53,920
And then they are loaded to so

47
00:02:51,599 --> 00:02:54,879
most of the stages are

48
00:02:53,920 --> 00:02:58,159
dynamically

49
00:02:54,879 --> 00:02:58,879
relocated are runtime. So,

50
00:02:58,159 --> 00:03:01,280


51
00:02:58,879 --> 00:03:02,000
if we want to load new files at given

52
00:03:01,280 --> 00:03:04,159
address,

53
00:03:02,000 --> 00:03:06,640
we have to figure out it.

54
00:03:04,159 --> 00:03:06,640
oOurselves

55
00:03:06,959 --> 00:03:13,519
every next put stage of coreboot starts

56
00:03:10,400 --> 00:03:16,080
in program function.

57
00:03:13,519 --> 00:03:19,840
So, let's set up a breakpoint

58
00:03:16,080 --> 00:03:19,840
on this function.

59
00:03:29,120 --> 00:03:36,239
It seems to work and let's continue.

60
00:03:33,120 --> 00:03:39,840
So, maybe first let's see that we still

61
00:03:36,239 --> 00:03:43,360
are in reset vector,

62
00:03:39,840 --> 00:03:44,879
let's continue. And we see a boot block

63
00:03:43,360 --> 00:03:47,760
already executed,

64
00:03:44,879 --> 00:03:49,040
so we kind of step it through first

65
00:03:47,760 --> 00:03:52,640
stage of

66
00:03:49,040 --> 00:03:57,360
booting and we see that

67
00:03:52,640 --> 00:04:00,400
the next stage is ROM stage.

68
00:03:57,360 --> 00:04:14,159
But, there are some problems

69
00:04:00,400 --> 00:04:17,840
if we want to continue in GDB.

70
00:04:14,159 --> 00:04:19,759
Let's list our function and let's look

71
00:04:17,840 --> 00:04:29,840
at the

72
00:04:19,759 --> 00:04:29,840
prog_struct.

73
00:04:35,680 --> 00:04:42,479
We can see a program function,

74
00:04:38,800 --> 00:04:45,680
it takes a struct_prog

75
00:04:42,479 --> 00:04:49,120
tas a parameter. So

76
00:04:45,680 --> 00:04:52,320
let's look at the type

77
00:04:49,120 --> 00:04:55,120
of a struct prog

78
00:04:52,320 --> 00:04:57,280
and what's interesting here in this

79
00:04:55,120 --> 00:05:00,320
struct of course,

80
00:04:57,280 --> 00:05:02,639
we don't discuss

81
00:05:00,320 --> 00:05:04,639
internals but this is just an example

82
00:05:02,639 --> 00:05:07,280
how we can do debugging and go through

83
00:05:04,639 --> 00:05:10,720
the stages. So, we see instruct prog

84
00:05:07,280 --> 00:05:14,000
we have struct region device

85
00:05:10,720 --> 00:05:16,800
and in that struct we have

86
00:05:14,000 --> 00:05:17,840
a struct region which has some offset

87
00:05:16,800 --> 00:05:20,720
and this offset

88
00:05:17,840 --> 00:05:22,840
points to the begining of the next

89
00:05:20,720 --> 00:05:27,199
stage.

90
00:05:22,840 --> 00:05:30,320
So, let's think how we can

91
00:05:27,199 --> 00:05:34,320
use that information

92
00:05:30,320 --> 00:05:34,320
to calculate new stage.

93
00:05:37,520 --> 00:05:41,680
On x86, parameters are passed on the

94
00:05:40,800 --> 00:05:45,120
stacks,

95
00:05:41,680 --> 00:05:48,320
usually from right to left.

96
00:05:45,120 --> 00:05:51,039
And call to function pushes also

97
00:05:48,320 --> 00:05:52,880
the return address to the stack. So,

98
00:05:51,039 --> 00:05:55,039
immediately after the entry

99
00:05:52,880 --> 00:05:58,000
the first parameter can be accessed

100
00:05:55,039 --> 00:06:00,880
using stack pointer +4,

101
00:05:58,000 --> 00:06:01,360
this example sets convenience

102
00:06:00,880 --> 00:06:04,560


103
00:06:01,360 --> 00:06:05,280
variable pointing which we can leverage

104
00:06:04,560 --> 00:06:08,160
here

105
00:06:05,280 --> 00:06:08,960
through some calculation in GDB. So,

106
00:06:08,160 --> 00:06:12,800
let's

107
00:06:08,960 --> 00:06:12,800
let's try to point to

108
00:06:12,880 --> 00:06:18,000
our structure prog.

109
00:06:18,720 --> 00:06:25,120
So, let's

110
00:06:22,240 --> 00:06:25,120
copy that

111
00:06:29,840 --> 00:06:35,840
and set in GDB.

112
00:06:43,039 --> 00:06:45,440
Okay,

113
00:06:47,280 --> 00:06:50,639
we can of course read the value of

114
00:06:49,360 --> 00:06:53,599
this offset

115
00:06:50,639 --> 00:06:56,560
by using print command, but what's most

116
00:06:53,599 --> 00:07:00,240
important we want to point to the

117
00:06:56,560 --> 00:07:04,000
region offset, which we saw

118
00:07:00,240 --> 00:07:07,039
in the structure. And read that value

119
00:07:04,000 --> 00:07:10,319
and of course add symbols files,

120
00:07:07,039 --> 00:07:11,440
of ROM stage to that address. What

121
00:07:10,319 --> 00:07:14,560
will help us

122
00:07:11,440 --> 00:07:17,360
in jumping between the stages,

123
00:07:14,560 --> 00:07:18,000
and using like the same symbol since

124
00:07:17,360 --> 00:07:21,280
the

125
00:07:18,000 --> 00:07:24,560
program function in all the stages

126
00:07:21,280 --> 00:07:26,800
like finish

127
00:07:24,560 --> 00:07:28,160
the stage start the next

128
00:07:26,800 --> 00:07:32,319
stage.

129
00:07:28,160 --> 00:07:36,160
And in every stage it is called the same.

130
00:07:32,319 --> 00:07:36,160
So, let's

131
00:07:37,440 --> 00:07:40,319
just

132
00:07:41,840 --> 00:07:45,840
print the address.

133
00:07:52,879 --> 00:07:59,599
And let's use that address

134
00:07:57,520 --> 00:08:01,919
for the purpose of adding the symbol

135
00:07:59,599 --> 00:08:01,919
file.

136
00:08:07,039 --> 00:08:13,860
Of course we confirming and now

137
00:08:10,319 --> 00:08:15,039
if we will continue, we should

138
00:08:13,860 --> 00:08:18,080


139
00:08:15,039 --> 00:08:20,160
break at the program of ROM

140
00:08:18,080 --> 00:08:22,400
stage.

141
00:08:20,160 --> 00:08:23,599
That's good, and of course what we see

142
00:08:22,400 --> 00:08:25,680
here

143
00:08:23,599 --> 00:08:27,360
in the other window is that their own

144
00:08:25,680 --> 00:08:30,800
stage started

145
00:08:27,360 --> 00:08:33,599
and continue and now we could

146
00:08:30,800 --> 00:08:35,680
continue the same procedure to obtain

147
00:08:33,599 --> 00:08:39,279
postcard

148
00:08:35,680 --> 00:08:43,919
address and load the postcard

149
00:08:39,279 --> 00:08:43,919
stage into the debugging process.

150
00:08:45,920 --> 00:08:50,640
Sometimes, it may be a good idea to

151
00:08:48,560 --> 00:08:53,519
remove symbols

152
00:08:50,640 --> 00:08:54,240
because if multiple stages use the same

153
00:08:53,519 --> 00:08:57,440


154
00:08:54,240 --> 00:09:01,279
symbols, we may hit some

155
00:08:57,440 --> 00:09:04,000
conflicts. But, in this case this is

156
00:09:01,279 --> 00:09:04,000
not the problem.

157
00:09:04,880 --> 00:09:11,360
And of course

158
00:09:08,160 --> 00:09:11,839
now when we loaded the ROM stage we can

159
00:09:11,360 --> 00:09:14,959
use

160
00:09:11,839 --> 00:09:17,760
ROM stage symbols and

161
00:09:14,959 --> 00:09:19,839
if we want to use symbols from

162
00:09:17,760 --> 00:09:20,480
further stages we of course have to load

163
00:09:19,839 --> 00:09:22,800
them

164
00:09:20,480 --> 00:09:23,760
in correct way providing address and so

165
00:09:22,800 --> 00:09:26,720
on.

166
00:09:23,760 --> 00:09:27,600
So, as I said like we can follow the

167
00:09:26,720 --> 00:09:30,399
same procedure

168
00:09:27,600 --> 00:09:31,360
until everything will boot.

169
00:09:30,399 --> 00:09:35,360
Alternatively,

170
00:09:31,360 --> 00:09:37,839
if debug level is very high

171
00:09:35,360 --> 00:09:39,040
we can redo just the breakpoint and

172
00:09:37,839 --> 00:09:42,080
observe the

173
00:09:39,040 --> 00:09:44,720
QEMU serial output.

174
00:09:42,080 --> 00:09:46,240
And in this case we showed how

175
00:09:44,720 --> 00:09:49,040
the postcard looked like and

176
00:09:46,240 --> 00:09:50,560
also on the terminal we saw the

177
00:09:49,040 --> 00:09:53,519
same.

178
00:09:50,560 --> 00:09:53,920
Of course, we are interested in the

179
00:09:53,519 --> 00:09:56,959
line

180
00:09:53,920 --> 00:10:00,160
started with loading module at X,

181
00:09:56,959 --> 00:10:02,079
where X is the memory location

182
00:10:00,160 --> 00:10:04,000
where we load the module.

183
00:10:02,079 --> 00:10:05,120
Unfortunately such line is not

184
00:10:04,000 --> 00:10:07,760
printed for

185
00:10:05,120 --> 00:10:09,360
boot block to ROM stage transition,

186
00:10:07,760 --> 00:10:11,600
because ROM stage

187
00:10:09,360 --> 00:10:12,480
is not compressed and is executed in

188
00:10:11,600 --> 00:10:16,160
place.

189
00:10:12,480 --> 00:10:19,760
So, such information

190
00:10:16,160 --> 00:10:22,959
is not going to the

191
00:10:19,760 --> 00:10:22,959
logs.


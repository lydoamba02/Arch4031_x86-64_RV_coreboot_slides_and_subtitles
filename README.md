# Architecture 4031: Introductory coreboot

This repository contain files to generate PDF presentations of [Open Security
Training2](https://ost2.fyi/) course [Architecture 4031: Introductory coreboot](https://ost2.fyi/Arch4031).

## Building

### Linux

Requirements:
* Python 3

```shell
git clone https://gitlab.com/opensecuritytraining/Arch4031_x86-64_RV_coreboot_slides_and_subtitles.git
cd Arch4031_x86-64_RV_coreboot_slides_and_subtitles
python3 -m http.server
```

Now you can open web browser to access `0.0.0.0:8000`, for example:

```shell
google-chrome 0.0.0.0:8000
```

![](images/presentations_list.png)

Open file with `.html` extension. That should render presentation for given
module.

![](images/rendered_presentation.png)

### Printing PDFs in Google Chrome

Click `Ctrl+P` and save as PDF.

![](images/chrome_printing_screen.png)

## TODO

* Automated PDF generation without opening web browser
